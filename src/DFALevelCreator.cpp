#include "DFALevelCreator.h"
#include "DFAAvatar.h"
#include "DFAProjectileManager.h"
#include "DFATank.h"
#include "DFAWorldData.h"
#include "DFAObjectTextDisplay.h"

using namespace Ogre;

DFAData::DFALevelCreator *DFAData::DFALevelCreator::mLevelCreator = 0;

DFAData::DFALevelCreator::~DFALevelCreator()
{
	
}

Ogre::Vector3 DFAData::DFALevelCreator::getLevelOffset(const int levelNumber)
{
	if(levelNumber >= 0 && levelNumber < mLevels.size())
		return mLevels[levelNumber]->terrainOffset;
	else
		return Vector3(0, 0, 0);
}

Ogre::String DFAData::DFALevelCreator::getTerrainNodeName(const int levelNumber)
{
	if(levelNumber >= 0 && levelNumber < mLevels.size())
		return mLevels[levelNumber]->nodeName;
	else
		return "";
}

void DFAData::DFALevelCreator::createLevel(const int levelNumber, Ogre::SceneManager *sceneMgr, Ogre::RenderWindow *renderWindow,
												OgreOggSound::OgreOggSoundManager *soundManager, Ogre::Camera *camera)
{
	//creating ProjectileManager
	//DFACore::DFAProjectileManager::getSingletonPtr();

	std::list<DFACore::DFAWorldObject*> &worldObjects = DFAData::DFAWorldData::getSingletonPtr()->getWorldObjects();

	sceneMgr->setAmbientLight(ColourValue(0.25, 0.25, 0.25));
	
	//creating terrain
	sceneMgr->setWorldGeometry("DesertTerrain.cfg");
/*
	String terrainNodeName = "DesertNode";
	OgreMax::OgreMaxScene *desertScene = new OgreMax::OgreMaxScene();
	desertScene->Load(0, "Desert.scene", renderWindow, OgreMax::OgreMaxScene::NO_OPTIONS, sceneMgr, sceneMgr->getRootSceneNode()->createChildSceneNode(terrainNodeName));

	Vector3 levelOffset(0, -500, 0);
	Ogre::SceneNode *desertNode = sceneMgr->getSceneNode(terrainNodeName);
	desertNode->scale(Vector3(500, 500, 500));
	desertNode->setPosition(levelOffset);
*/
/*
	LevelData *levelData = new LevelData();
	levelData->nodeName = terrainNodeName;
	levelData->terrainOffset = levelOffset;
	mLevels.push_back(levelData);
*/
	//add hangars
	StaticGeometry *staticGeometry = sceneMgr->createStaticGeometry("HangarArea");

	OgreMax::OgreMaxScene *hangarScene = new OgreMax::OgreMaxScene();
	hangarScene->SetNamePrefix(StringConverter::toString(0) + "_");
	hangarScene->Load("Hangar.scene", renderWindow, OgreMax::OgreMaxScene::NO_OPTIONS, sceneMgr, sceneMgr->getRootSceneNode()->createChildSceneNode("HangarNode_0"));
	Ogre::SceneNode *hangarNode = (SceneNode*)sceneMgr->getRootSceneNode()->removeChild("HangarNode_0");
	//hangarNode->setPosition(Vector3(1000, 50, -4900));
	hangarNode->setPosition(Vector3(7790, 0, 10164));
	//hangarNode->setScale(Vector3(0.2, 0.2, 0.2));
	staticGeometry->addSceneNode(hangarNode);

	//hangarScene = new OgreMax::OgreMaxScene();
	hangarScene->SetNamePrefix(StringConverter::toString(1) + "_");
	hangarScene->Load("Hangar.scene", renderWindow, OgreMax::OgreMaxScene::NO_OPTIONS, sceneMgr, sceneMgr->getRootSceneNode()->createChildSceneNode("HangarNode_1"));
	hangarNode = (SceneNode*)sceneMgr->getRootSceneNode()->removeChild("HangarNode_1");
	//hangarNode->setPosition(Vector3(800, 50, -4900));
	hangarNode->setPosition(Vector3(7870, 0, 10144));
	//hangarNode->setScale(Vector3(0.2, 0.2, 0.2));
	staticGeometry->addSceneNode(hangarNode);

	//build all static objects
	staticGeometry->build();

	//create the sky with sun
	sceneMgr->setSkyDome(true, "Sky", 2, 4, 10000);
	Light *sun = sceneMgr->createLight("SunLight");
	sun->setType(Light::LT_DIRECTIONAL);
	sun->setDirection(Vector3(0, -1, -1));
	sun->setDiffuseColour(ColourValue::White);
	sun->setSpecularColour(ColourValue::White);
	

	/*
	SceneNode *node = mSceneMgr->getRootSceneNode()->createChildSceneNode("camNode1", Vector3(0,0,500));
	node->lookAt(Vector3::ZERO, Node::TS_WORLD);
	node->attachObject(mCamera);
	node->attachObject(mSoundManager->getListener());
	*/

	//add avatar
	DFACore::DFAAvatar *avatar = DFACore::DFAAvatar::getSingletonPtr();
	avatar->createObject("Avatar", 0, "Mi24.scene", renderWindow, OgreMax::OgreMaxScene::NO_OPTIONS, sceneMgr, DFACore::AVATAR);
	//avatar->setStartingPosition(Vector3(900, 80, -4970));
	avatar->setPosition(Vector3(7922, 1, 10174));
	avatar->lookAt(Vector3::ZERO);
	avatar->setWingPayload(1, new DFACore::DFAWeaponContainer(sceneMgr, DFACore::ROCKET_80MM, 20));
	avatar->setWingPayload(2, new DFACore::DFAWeaponContainer(sceneMgr, DFACore::ROCKET_80MM, 20));
	avatar->setWingPayload(3, new DFACore::DFAWeaponContainer(sceneMgr, DFACore::AT6_SPIRAL, 2));
	worldObjects.push_back(avatar);

	//add camera
	SceneNode *camNode = avatar->getCameraNode();
	camNode->attachObject(camera);
	camNode->attachObject(soundManager->getListener());

	//add tanks
	DFAAI::DFATank *abramsTank = new DFAAI::DFATank();
	abramsTank->createObject("M1A2 Abrams", 0, "Abrams3.scene", renderWindow, OgreMax::OgreMaxScene::NO_OPTIONS, sceneMgr, DFACore::ENEMY);
	//abramsTank->setStartingPosition(Vector3(0, 100, 100));
	//abramsTank->setStartingPosition(Vector3(11838, 1, 14459));
	abramsTank->setPosition(Vector3(8102, 1, 10274));
	worldObjects.push_back(abramsTank);

	abramsTank = new DFAAI::DFATank();
	abramsTank->createObject("M1A2 Abrams", 1, "Abrams3.scene", renderWindow, OgreMax::OgreMaxScene::NO_OPTIONS, sceneMgr, DFACore::ENEMY);
	//abramsTank->setStartingPosition(Vector3(0, 100, 100));
	//abramsTank->setStartingPosition(Vector3(11838, 1, 14459));
	abramsTank->setPosition(Vector3(8112, 1, 10274));
	worldObjects.push_back(abramsTank);

	abramsTank = new DFAAI::DFATank();
	abramsTank->createObject("M1A2 Abrams", 2, "Abrams3.scene", renderWindow, OgreMax::OgreMaxScene::NO_OPTIONS, sceneMgr, DFACore::ENEMY);
	//abramsTank->setStartingPosition(Vector3(0, 100, 100));
	//abramsTank->setStartingPosition(Vector3(11838, 1, 14459));
	abramsTank->setPosition(Vector3(8122, 1, 10274));
	worldObjects.push_back(abramsTank);

	//load all weapon meshes
	ResourceGroupManager::getSingletonPtr()->createResourceGroup("preload");
	ResourceGroupManager::getSingletonPtr()->declareResource("S-8Rocket.mesh", "Mesh", "preload");
	ResourceGroupManager::getSingletonPtr()->initialiseResourceGroup("preload");
/*
	abramsTank = new DFAAI::DFATank();
	abramsTank->createObject("M1A2 Abrams", 1, "Abrams2.scene", renderWindow, OgreMax::OgreMaxScene::NO_OPTIONS, sceneMgr);
	abramsTank->setStartingPosition(Vector3(0, 100, 200));
	worldObjects.push_back(abramsTank);
*/





	//just random stuff
	/*Light *light = sceneMgr->createLight("Light1");
	light->setType(Light::LT_POINT);
	light->setPosition(Vector3(250, 150, 250));
	light->setDiffuseColour(ColourValue::White);
	light->setSpecularColour(ColourValue::White);
	*/

	//just temporary -> need to change
	//std::list<DFACore::DFAWorldObject*> temp;
}

DFAData::DFALevelCreator* DFAData::DFALevelCreator::getSingletonPtr(void)
{
	if(!mLevelCreator)
	{
		mLevelCreator = new DFAData::DFALevelCreator();
	}

	return mLevelCreator;
	
	//return ms_Singleton;
}