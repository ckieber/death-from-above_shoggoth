#include "DFAProjectile.h"
#include "DFAProjectileManager.h"
#include "DFAWorldData.h"

#define MAX_RANGE 2000
#define MAX_SPEED 960
#define DAMAGE 5

using namespace Ogre;

DFACore::DFAProjectile::DFAProjectile(Ogre::SceneManager *sceneManager, const int id, const Ogre::String &name)
{
	mDestroyed = false;
	mPlayDestroyedAnimation = false;
	mId = id;
	mName = name;

	mSpeed = MAX_SPEED;
	mDamage = DAMAGE;
	mDirection = Vector3::ZERO;
	mBillboard = NULL;
	mDistanceTraveled = 0;
	mCollisionTool = new DFATools::DFACollisionTool(sceneManager);
}

DFACore::DFAProjectile::~DFAProjectile()
{
	if(mCollisionTool)
		delete mCollisionTool;
}

void DFACore::DFAProjectile::fire(const Ogre::Vector3 startPosition, const Ogre::Vector3 targetPosition, const Ogre::Vector3 ownerDirection)
{
	mStartPosition = startPosition;
	mTargetPosition = targetPosition;
	mDirection = targetPosition - startPosition;
	mDirection.normalise();
	mBillboard = DFACore::DFAProjectileManager::getSingletonPtr()->addBillboard(startPosition);
	
	//check if we hit something
	Ogre::MovableObject *target;
	const uint32 queryFlags = (~AVATAR) & (ENEMY | STATIONARY);
	Ogre::String hitTarget = mCollisionTool->getCollisionTargetType(startPosition, targetPosition, target, 0.05, 0, queryFlags);
	if(hitTarget == "SceneObject")
		DFAData::DFAWorldData::getSingletonPtr()->getSceneObjectFromMesh(target)->decreaseHealth(mDamage);
}

void DFACore::DFAProjectile::update(const Ogre::Real frameRate, Ogre::SceneManager *sceneManager)
{
	//get old and new position
	const Vector3 oldPosition = mBillboard->getPosition();
	const Real distance = mSpeed * frameRate;
	const Vector3 newPosition = oldPosition + (distance * mDirection);
	mDistanceTraveled += distance;

	bool destroy = false;
	if(mDistanceTraveled > MAX_RANGE)
		destroy = true;

	if(destroy)
	{
		DFACore::DFAProjectileManager::getSingletonPtr()->removeBillboard(mBillboard);
		mDestroyed = true;
	}
	else
	{
		mBillboard->setPosition(newPosition);
	}


	/*
	DFACore::DFAProjectileManager *projectileManager = DFACore::DFAProjectileManager::getSingletonPtr();
	
	mBBElementHead.position = mTargetPosition;
	mBBElementTail.position = mStartPosition;
	projectileManager->getBBChain()->updateChainElement(mId, 0, mBBElementHead);
	projectileManager->getBBChain()->updateChainElement(mId, 1, mBBElementTail);

	mLastPosition = mTargetPosition;
	*/
}

Ogre::String DFACore::DFAProjectile::getName(void)
{
	return mName;
}

bool DFACore::DFAProjectile::isDestroyed(void)
{
	return mDestroyed;
}