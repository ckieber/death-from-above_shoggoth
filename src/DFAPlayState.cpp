#include <Ogre.h>

#include "DFAPlayState.h"
#include "DFALevelCreator.h"
#include "DFADotSceneLoader.h"
#include "DFATank.h"
#include "DFAObjectTextDisplay.h"

using namespace Ogre;

DFACore::DFAPlayState DFACore::DFAPlayState::mPlayState;

void DFACore::DFAPlayState::enter()
{
	mRoot = Root::getSingletonPtr();
	mKeyboard = DFAInput::DFAInputManager::getSingletonPtr()->getKeyboard();
	mMouse = DFAInput::DFAInputManager::getSingletonPtr()->getMouse();

	//mSceneMgr = mRoot->createSceneManager(SceneType::ST_GENERIC);
    //mSceneMgr = mRoot->createSceneManager("OctreeSceneManager", "MainSceneManager");
	mSceneMgr = mRoot->createSceneManager(ST_EXTERIOR_CLOSE, "MainSceneManager");

	mCamera = mSceneMgr->createCamera("PlayCamera");
	mCamera->setNearClipDistance(1);
	mViewport = mRoot->getAutoCreatedWindow()->addViewport(mCamera);
	mCamera->setAspectRatio(Real(mViewport->getActualWidth())/Real(mViewport->getActualHeight()));
	//mCamera->setPosition(Vector3(0,0,500));
	//mCamera->lookAt(Vector3::ZERO);
	//mViewport->setBackgroundColour(ColourValue(0.0, 0.0, 1.0));

	mSoundManager = OgreOggSound::OgreOggSoundManager::getSingletonPtr();
	//mSoundManager->init();
	mSoundManager->setDistanceModel(AL_LINEAR_DISTANCE);

	createScene();

	mDoorPressed = false;
	mGearPressed = false;
	mTargetSystemPressed = false;
	mNextTargetPressed = false;
	mNextWeaponPressed = false;
	mExitGame = false;
}

void DFACore::DFAPlayState::exit()
{
	if(mSoundManager)
		delete mSoundManager;

	mSceneMgr->clearScene();
	mSceneMgr->destroyAllCameras();
	mRoot->getAutoCreatedWindow()->removeAllViewports();
}

void DFACore::DFAPlayState::pause()
{
}

void DFACore::DFAPlayState::resume()
{
}

bool DFACore::DFAPlayState::keyClicked(const OIS::KeyEvent &e)
{
	return true;
}

bool DFACore::DFAPlayState::keyPressed(const OIS::KeyEvent &e)
{
	const bool shiftPressed = DFAInput::DFAInputManager::getSingletonPtr()->getKeyboard()->isKeyDown(OIS::KC_LSHIFT);
	switch(e.key)
	{
		case OIS::KC_ESCAPE:
			mExitGame = true;
			break;
		case OIS::KC_UP:
			mAvatar->accelerate(true);
			break;
		case OIS::KC_DOWN:
			mAvatar->brake(true);
			break;
		case OIS::KC_LEFT:
			mAvatar->moveLeft(true);
			break;
		case OIS::KC_RIGHT:
			mAvatar->moveRight(true);
			break;
		case OIS::KC_S:
			if(!mTargetSystemPressed)
			{
				mAvatar->activateTargetSystem(!mAvatar->isTargetSystemActivated());
				mTargetSystemPressed = true;
			}
			break;
		case OIS::KC_PGDOWN:
		case OIS::KC_E:
			mAvatar->moveDown(true);
			break;
		case OIS::KC_PGUP:
		case OIS::KC_Q:
			mAvatar->moveUp(true);
			break;
		case OIS::KC_X:
			mAvatar->rotateRight(true);
			break;
		case OIS::KC_Z:
			mAvatar->rotateLeft(true);
			break;
		case OIS::KC_D:
			if(!mDoorPressed)
			{
				mAvatar->doorsOpen(!mAvatar->isDoorOpen());
				mDoorPressed = true;
			}
			break;
		case OIS::KC_U:
			if(!mGearPressed)
			{
				mAvatar->gearUp(!mAvatar->isGearUp());
				mGearPressed = true;
			}
			break;
		case OIS::KC_BACK:
			if(!mNextTargetPressed)
			{
				if(shiftPressed)
					mAvatar->previousTarget();
				else
					mAvatar->nextTarget();
				mNextTargetPressed = true;
			}
			break;
		case OIS::KC_RETURN:
			if(!mNextWeaponPressed)
			{
				if(shiftPressed)
					mAvatar->previousWeapon();
				else
					mAvatar->nextWeapon();
				mNextWeaponPressed = true;
			}
			break;
		case OIS::KC_SPACE:
			mAvatar->shoot(true, mSceneMgr);
			break;
		default:
			break;
	}
	return !mExitGame;
}

bool DFACore::DFAPlayState::keyReleased(const OIS::KeyEvent &e)
{
	switch (e.key)
	{
		case OIS::KC_UP:
			mAvatar->accelerate(false);
			break;
		case OIS::KC_DOWN:
			mAvatar->brake(false);
			break;
		case OIS::KC_LEFT:
			mAvatar->moveLeft(false);
			break;
		case OIS::KC_RIGHT:
			mAvatar->moveRight(false);
			break;
		case OIS::KC_S:
			mTargetSystemPressed = false;
			break;
		case OIS::KC_PGDOWN:
		case OIS::KC_E:
			mAvatar->moveDown(false);
			break;
		case OIS::KC_PGUP:
		case OIS::KC_Q:
			mAvatar->moveUp(false);
			break;
		case OIS::KC_X:
			mAvatar->rotateRight(false);
			break;
		case OIS::KC_Z:
			mAvatar->rotateLeft(false);
			break;
		case OIS::KC_D:
			mDoorPressed = false;
			break;
		case OIS::KC_U:
			mGearPressed = false;
			break;
		case OIS::KC_BACK:
			mNextTargetPressed = false;
			break;
		case OIS::KC_RETURN:
			mNextWeaponPressed = false;
			break;
		case OIS::KC_SPACE:
			mAvatar->shoot(false, NULL);
		default:
			break;
		}
		

	return true;
}

bool DFACore::DFAPlayState::mouseMoved(const OIS::MouseEvent &e)
{
	return true;
}

bool DFACore::DFAPlayState::mousePressed(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
	/*
	Light *light = mSceneMgr->getLight("Light1");
	switch(id)
	{
		case OIS::MB_Left:
			light->setVisible(!light->isVisible());
			break;
		default:
			break;
	}
	*/
	return true;
}

bool DFACore::DFAPlayState::mouseReleased(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
	return true;
}

bool DFACore::DFAPlayState::frameStarted(const FrameEvent &evt)
{
	//DFAInput::DFAInputManager::getSingletonPtr()->capture();

	//mSoundManager->update(evt.timeSinceLastFrame);

	/*
	//update position and animation for all worldObjects
	if(mWorldObjects.size() > 0)
	{
		std::list<DFAWorldObject*>::iterator iter = mWorldObjects.begin();
		for(;iter != mWorldObjects.end(); iter++)
		{
			(*iter)->updatePosition(evt);
			(*iter)->updateAnimations(evt);
		}
	}
	*/

	//mAvatar->setTargetList(mWorldData->updateTargetList(evt, mWorldObjects, mSceneMgr));
	//mText->update();

	return true;
}

bool DFACore::DFAPlayState::frameEnded(const FrameEvent &evt)
{
	return !mExitGame;
}

void DFACore::DFAPlayState::captureInput(void)
{
	DFAInput::DFAInputManager::getSingletonPtr()->capture();
}

void DFACore::DFAPlayState::processLogic(const Real frameRate)
{
	mSoundManager->update(frameRate);
	std::list<DFAWorldObject*> &worldObjects = mWorldData->getWorldObjects();

	//update position, animation, and more for all worldObjects
	if(worldObjects.size() > 0)
	{
		std::list<DFAWorldObject*>::iterator iter = worldObjects.begin();
		for(;iter != worldObjects.end();)
		{
			(*iter)->update(frameRate, mSceneMgr);
			
			if((*iter)->isDestroyed() && dynamic_cast<DFACore::DFAWeapon*>((*iter)))
			{
				delete *iter;
				iter = worldObjects.erase(iter);
			}
			else
				iter++;
			
		}
	}

	mAvatar->updateTargetList();
}

void DFACore::DFAPlayState::createScene()
{
	mWorldData = DFAData::DFAWorldData::getSingletonPtr();

	DFAData::DFALevelCreator *levelCreator = DFAData::DFALevelCreator::getSingletonPtr();
	levelCreator->createLevel(0, mSceneMgr, mRoot->getAutoCreatedWindow(), mSoundManager, mCamera);
	mAvatar = (DFAAvatar*) mWorldData->getWorldObjects().front();

	//std::list<DFAWorldObject*>::iterator iter = mWorldObjects.begin();
	//iter++;

	//mText = new DFATools::ObjectTextDisplay(((DFAAI::DFATank*)(*iter))->getTextDisplayEntity(mSceneMgr),
		//mCamera);
	//mText->enable(true);
    //mText->setText("Target");
	
	/*
	//mSceneMgr->setWorldGeometry("terrain.cfg");
	
	SceneNode *node = mSceneMgr->getRootSceneNode()->createChildSceneNode("camNode1", Vector3(0,0,500));
	node->lookAt(Vector3::ZERO, Node::TS_WORLD);
	node->attachObject(mCamera);
	node->attachObject(mSoundManager->getListener());
	

	//mSceneMgr->setAmbientLight(ColourValue(0.25, 0.25, 0.25));

	mAvatar = DFAAvatar::getSingletonPtr();
	mAvatar->createObject(0, "Mi24.scene", mRoot->getAutoCreatedWindow(), OgreMax::OgreMaxScene::NO_OPTIONS, mSceneMgr);
	/*mWorldObjects.push_back(mAvatar);

	
	//add camera
	SceneNode *camNode = mAvatar->getCameraNode();
	camNode->attachObject(mCamera);
	camNode->attachObject(mSoundManager->getListener());
	

	


	//just random stuff
	Light *light = mSceneMgr->createLight("Light1");
	light->setType(Light::LT_POINT);
	light->setPosition(Vector3(250, 150, 250));
	light->setDiffuseColour(ColourValue::White);
	light->setSpecularColour(ColourValue::White);
	*/
}