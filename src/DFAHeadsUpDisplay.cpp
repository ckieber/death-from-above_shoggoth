#include "DFAHeadsUpDisplay.h"

using namespace Ogre;

DFAData::DFAHeadsUpDisplay *DFAData::DFAHeadsUpDisplay::mHeadsUpDisplay = 0;

DFAData::DFAHeadsUpDisplay::DFAHeadsUpDisplay()
{
	
}

DFAData::DFAHeadsUpDisplay::~DFAHeadsUpDisplay()
{
	
}

DFAData::DFAHeadsUpDisplay* DFAData::DFAHeadsUpDisplay::getSingletonPtr(void)
{
	if(!mHeadsUpDisplay)
	{
		mHeadsUpDisplay = new DFAData::DFAHeadsUpDisplay();
	}

	return mHeadsUpDisplay;
}