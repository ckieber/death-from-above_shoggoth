#include "DFAAvatar.h"
#include "DFAWorldData.h"
#include "OgreOggSound.h"
#include <sstream>

#define ACCELERATION 2
#define PITCH_ANGLE 15
#define ROLL_ANGLE 20
#define PITCH_SPEED 0.25
#define ROLL_SPEED 0.35
#define YAW_SPEED 0.15
#define MAX_SPEED 94//250
#define MAX_CLIMB_SPEED 15//50
#define TURN_SPEED_THRESHOLD 14//50
#define TURN_SPEED 0.35

#define CAMERA_DIST 30
#define CAMERA_HEIGHT 10
#define CAMERA_LOOK_AT 20

#define	WEAPON_MAX 4
#define MAX_TARGET_RANGE 1000
#define CLOSEST_TARGET_DISTANCE 100
#define DEFAULT_TARGET_DISTANCE 500
#define RADAR_ANGLE 150

#define CROSSHAIR_START_X 0.5
#define	CROSSHAIR_START_Y 0.2
#define CROSSHAIR_OFFSET_Y 0.4

using namespace Ogre;

DFACore::DFAAvatar *DFACore::DFAAvatar::mAvatar = 0;
//template<> DFACore::DFAAvatar *Singleton<DFACore::DFAAvatar>::ms_Singleton = 0;

DFACore::DFAAvatar::DFAAvatar()
{
	mHeightThreadHandle = 0;
	mHealth = 10000;
	mDestroyed = false;
	mPlayDestroyedAnimation = false;

	mSpeed = 0;
	mHeight = 0;
	mClimb = Vector3::ZERO;
	mDirection = Vector3::ZERO;

	mAnglePitch = 0;
	mAngleRoll = 0;
	mAngleYaw = 0;

	mPitchSpeed = PITCH_SPEED;
	mBrake = false;
	mAccelerate = false;

	mRollSpeed = ROLL_SPEED;
	mRollLeft = false;
	mRollRight = false;

	mYawSpeed = YAW_SPEED;
	mYawLeft = false;
	mYawRight = false;

	mDoorsOpen = true;
	mGearUp = false;

	mCurrentWeapon = 0;
	mShoot = false;
	mTargetSystem = true;
	mShootLeftRight = false;
	mTargetFriendlies = false;
	mCurrentTarget = NULL;
}

DFACore::DFAAvatar::~DFAAvatar()
{
	if(mDotSceneNode)
		mDotSceneNode->Destroy();

	if(mCameraNode)
		delete mCameraNode;
	if(mObjectNode)
		delete mObjectNode;
	if(mMovementNode)
		delete mMovementNode;
	
	if(mCollisionTool)
		delete mCollisionTool;
}

void DFACore::DFAAvatar::loadAnimations(Ogre::SceneManager *sceneManager)
{
	AnimationState *state = mDotSceneNode->GetAnimationState("mainConst");
	state->setEnabled(true);
	state->setLoop(true);
	mAnimationStates.push_back(state);

	Entity *mainRotor = sceneManager->getEntity(mNamePrefix + "main_rotor");
	state = mainRotor->getAnimationState("mainLift");
	state->setEnabled(true);
	state->setLoop(false);
	mAnimationStates.push_back(state);

	state = mDotSceneNode->GetAnimationState("tailConst");
	state->setEnabled(true);
	state->setLoop(true);
	mAnimationStates.push_back(state);

	//door closing animation
	state = mDotSceneNode->GetAnimationState("doorLeftLowClose");
	mAnimationStates.push_back(state);
	state = mDotSceneNode->GetAnimationState("doorLeftHighClose");
	mAnimationStates.push_back(state);
	state = mDotSceneNode->GetAnimationState("doorRightLowClose");
	mAnimationStates.push_back(state);
	state = mDotSceneNode->GetAnimationState("doorRightHighClose");
	mAnimationStates.push_back(state);

	//door opening animation
	state = mDotSceneNode->GetAnimationState("doorLeftLowOpen");
	mAnimationStates.push_back(state);
	state = mDotSceneNode->GetAnimationState("doorLeftHighOpen");
	mAnimationStates.push_back(state);
	state = mDotSceneNode->GetAnimationState("doorRightLowOpen");
	mAnimationStates.push_back(state);
	state = mDotSceneNode->GetAnimationState("doorRightHighOpen");
	mAnimationStates.push_back(state);

	//gear up animations
	state = mDotSceneNode->GetAnimationState("rightWheelUp");
	mAnimationStates.push_back(state);
	state = mDotSceneNode->GetAnimationState("rightWheelCoverUp");
	mAnimationStates.push_back(state);
	state = mDotSceneNode->GetAnimationState("leftWheelUp");
	mAnimationStates.push_back(state);
	state = mDotSceneNode->GetAnimationState("leftWheelCoverUp");
	mAnimationStates.push_back(state);
	state = mDotSceneNode->GetAnimationState("frontWheelUp");
	mAnimationStates.push_back(state);

	//gear down animations
	state = mDotSceneNode->GetAnimationState("rightWheelDown");
	mAnimationStates.push_back(state);
	state = mDotSceneNode->GetAnimationState("rightWheelCoverDown");
	mAnimationStates.push_back(state);
	state = mDotSceneNode->GetAnimationState("leftWheelDown");
	mAnimationStates.push_back(state);
	state = mDotSceneNode->GetAnimationState("leftWheelCoverDown");
	mAnimationStates.push_back(state);
	state = mDotSceneNode->GetAnimationState("frontWheelDown");
	mAnimationStates.push_back(state);

	closeDoors();
}

void DFACore::DFAAvatar::openDoors()
{
	int count = 0;
	std::list<Ogre::AnimationState*>::iterator iter = mAnimationStates.begin();
	for(;iter != mAnimationStates.end(); iter++)
	{
		if((*iter)->getAnimationName() == "doorLeftLowClose")
		{
			(*iter)->setEnabled(false);
			count++;
		}
		else if((*iter)->getAnimationName() == "doorLeftHighClose")
		{
			(*iter)->setEnabled(false);
			count++;
		}
		else if((*iter)->getAnimationName() == "doorRightLowClose")
		{
			(*iter)->setEnabled(false);
			count++;
		}
		else if((*iter)->getAnimationName() == "doorRightHighClose")
		{
			(*iter)->setEnabled(false);
			count++;
		}

		if(count >= 4)
			break;
	}

	count = 0;
	iter = mAnimationStates.begin();
	for(;iter != mAnimationStates.end(); iter++)
	{
		if((*iter)->getAnimationName() == "doorLeftLowOpen")
		{
			(*iter)->setEnabled(true);
			count++;
		}
		else if((*iter)->getAnimationName() == "doorLeftHighOpen")
		{
			(*iter)->setEnabled(true);
			count++;
		}
		else if((*iter)->getAnimationName() == "doorRightLowOpen")
		{
			(*iter)->setEnabled(true);
			count++;
		}
		else if((*iter)->getAnimationName() == "doorRightHighOpen")
		{
			(*iter)->setEnabled(true);
			count++;
		}

		if(count >= 4)
			break;
	}

	mDoorsOpen = true;
}

void DFACore::DFAAvatar::closeDoors()
{
	int count = 0;
	std::list<Ogre::AnimationState*>::iterator iter = mAnimationStates.begin();
	for(;iter != mAnimationStates.end(); iter++)
	{
		if((*iter)->getAnimationName() == "doorLeftLowOpen")
		{
			(*iter)->setEnabled(false);
			count++;
		}
		else if((*iter)->getAnimationName() == "doorLeftHighOpen")
		{
			(*iter)->setEnabled(false);
			count++;
		}
		else if((*iter)->getAnimationName() == "doorRightLowOpen")
		{
			(*iter)->setEnabled(false);
			count++;
		}
		else if((*iter)->getAnimationName() == "doorRightHighOpen")
		{
			(*iter)->setEnabled(false);
			count++;
		}

		if(count >= 4)
			break;
	}

	count = 0;
	iter = mAnimationStates.begin();
	for(;iter != mAnimationStates.end(); iter++)
	{
		if((*iter)->getAnimationName() == "doorLeftLowClose")
		{
			(*iter)->setEnabled(true);
			count++;
		}
		else if((*iter)->getAnimationName() == "doorLeftHighClose")
		{
			(*iter)->setEnabled(true);
			count++;
		}
		else if((*iter)->getAnimationName() == "doorRightLowClose")
		{
			(*iter)->setEnabled(true);
			count++;
		}
		else if((*iter)->getAnimationName() == "doorRightHighClose")
		{
			(*iter)->setEnabled(true);
			count++;
		}

		if(count >= 4)
			break;
	}

	mDoorsOpen = false;
}

bool DFACore::DFAAvatar::isDoorOpen() const
{
	return mDoorsOpen;
}

void DFACore::DFAAvatar::doorsOpen(bool open)
{
	if(open && !mDoorsOpen)
		openDoors();
	else if(!open && mDoorsOpen)
		closeDoors();
}

void DFACore::DFAAvatar::gearUp()
{
	int count = 0;
	std::list<Ogre::AnimationState*>::iterator iter = mAnimationStates.begin();
	for(;iter != mAnimationStates.end(); iter++)
	{
		if((*iter)->getAnimationName() == "rightWheelDown")
		{
			(*iter)->setEnabled(false);
			count++;
		}
		else if((*iter)->getAnimationName() == "rightWheelCoverDown")
		{
			(*iter)->setEnabled(false);
			count++;
		}
		else if((*iter)->getAnimationName() == "leftWheelDown")
		{
			(*iter)->setEnabled(false);
			count++;
		}
		else if((*iter)->getAnimationName() == "leftWheelCoverDown")
		{
			(*iter)->setEnabled(false);
			count++;
		}
		else if((*iter)->getAnimationName() == "frontWheelDown")
		{
			(*iter)->setEnabled(false);
			count++;
		}

		if(count >= 5)
			break;
	}

	count = 0;
	iter = mAnimationStates.begin();
	for(;iter != mAnimationStates.end(); iter++)
	{
		if((*iter)->getAnimationName() == "rightWheelUp")
		{
			(*iter)->setEnabled(true);
			count++;
		}
		else if((*iter)->getAnimationName() == "rightWheelCoverUp")
		{
			(*iter)->setEnabled(true);
			count++;
		}
		else if((*iter)->getAnimationName() == "leftWheelUp")
		{
			(*iter)->setEnabled(true);
			count++;
		}
		else if((*iter)->getAnimationName() == "leftWheelCoverUp")
		{
			(*iter)->setEnabled(true);
			count++;
		}
		else if((*iter)->getAnimationName() == "frontWheelUp")
		{
			(*iter)->setEnabled(true);
			count++;
		}

		if(count >= 5)
			break;
	}

	mGearUp = true;
}

void DFACore::DFAAvatar::gearDown()
{
	int count = 0;
	std::list<Ogre::AnimationState*>::iterator iter = mAnimationStates.begin();
	for(;iter != mAnimationStates.end(); iter++)
	{
		if((*iter)->getAnimationName() == "rightWheelUp")
		{
			(*iter)->setEnabled(false);
			count++;
		}
		else if((*iter)->getAnimationName() == "rightWheelCoverUp")
		{
			(*iter)->setEnabled(false);
			count++;
		}
		else if((*iter)->getAnimationName() == "leftWheelUp")
		{
			(*iter)->setEnabled(false);
			count++;
		}
		else if((*iter)->getAnimationName() == "leftWheelCoverUp")
		{
			(*iter)->setEnabled(false);
			count++;
		}
		else if((*iter)->getAnimationName() == "frontWheelUp")
		{
			(*iter)->setEnabled(false);
			count++;
		}

		if(count >= 5)
			break;
	}

	count = 0;
	iter = mAnimationStates.begin();
	for(;iter != mAnimationStates.end(); iter++)
	{
		if((*iter)->getAnimationName() == "rightWheelDown")
		{
			(*iter)->setEnabled(true);
			count++;
		}
		else if((*iter)->getAnimationName() == "rightWheelCoverDown")
		{
			(*iter)->setEnabled(true);
			count++;
		}
		else if((*iter)->getAnimationName() == "leftWheelDown")
		{
			(*iter)->setEnabled(true);
			count++;
		}
		else if((*iter)->getAnimationName() == "leftWheelCoverDown")
		{
			(*iter)->setEnabled(true);
			count++;
		}
		else if((*iter)->getAnimationName() == "frontWheelDown")
		{
			(*iter)->setEnabled(true);
			count++;
		}

		if(count >= 5)
			break;
	}

	mGearUp = false;
}

bool DFACore::DFAAvatar::isGearUp() const
{
	return mGearUp;
}

void DFACore::DFAAvatar::gearUp(bool up)
{
	if(up && !mGearUp)
		gearUp();
	else if(!up && mGearUp)
		gearDown();
}

void DFACore::DFAAvatar::loadSounds()
{
	OgreOggSound::OgreOggISound *engineSound = OgreOggSound::OgreOggSoundManager::getSingletonPtr()->createSound("hindEngine", "Hind_Engine.ogg", true, true, false);
	mObjectNode->attachObject(engineSound);
	engineSound->setMaxDistance(2000);
	//engineSound->setReferenceDistance(250);
	engineSound->play();
}

void DFACore::DFAAvatar::initializeHUD(void)
{
	//get HUD elements
	mCrosshairOverlay = OverlayManager::getSingleton().getByName("CrosshairOverlay");
	mCrosshair = OverlayManager::getSingleton().getOverlayElement("CrosshairPanel");
	mCrosshairOverlay->show();

	mCrosshairBoxOverlay = OverlayManager::getSingleton().getByName("CrosshairBoxOverlay");
	mCrosshairBox = OverlayManager::getSingleton().getOverlayElement("CrosshairBoxPanel");
	mCrosshairBoxOverlay->show();

	mHUDOverlay = OverlayManager::getSingleton().getByName("HUDOverlay");
	mSpeedOverlayElement = OverlayManager::getSingleton().getOverlayElement("FlightSpeed");
	mHeightOverlayElement = OverlayManager::getSingleton().getOverlayElement("FlightHeight");
	mTargetNameOverlayElement = OverlayManager::getSingleton().getOverlayElement("TargetName");
	mWeaponNameOverlayElement = OverlayManager::getSingleton().getOverlayElement("WeaponName");
	mWeaponAmmoOverlayElement = OverlayManager::getSingleton().getOverlayElement("WeaponAmmo");
	mHUDOverlay->show();

	//get screen width and height
	const int screenWidth = Root::getSingletonPtr()->getAutoCreatedWindow()->getWidth();
	const int screenHeight = Root::getSingletonPtr()->getAutoCreatedWindow()->getHeight();

	//set dimensions for crosshair
	const Real crosshairWidth = mCrosshair->getWidth() / screenWidth;
	const Real crosshairHeight = mCrosshair->getHeight() / screenHeight;
	mCrosshair->setDimensions(crosshairWidth, crosshairHeight);

	//set dimensions for crosshair box
	const Real crosshairBoxWidth = mCrosshairBox->getWidth() / screenWidth;
	const Real crosshairBoxHeight = mCrosshairBox->getHeight() / screenHeight;
	mCrosshairBox->setDimensions(crosshairBoxWidth, crosshairBoxHeight);

	//change to relative mode
	mCrosshair->setMetricsMode(GMM_RELATIVE);
	mCrosshairBox->setMetricsMode(GMM_RELATIVE);
}

void DFACore::DFAAvatar::updateAnimations(const Ogre::Real frameRate)
{
	if(mAnimationStates.size() > 0)
	{
		std::list<AnimationState*>::iterator iter = mAnimationStates.begin();
		for(;iter != mAnimationStates.end(); iter++)
		{
			(*iter)->addTime(frameRate);// * 0.8);
		}
	}
}

void DFACore::DFAAvatar::createObject(const Ogre::String name, const int id, const Ogre::String &sceneName, Ogre::RenderWindow *renderWindow,
			OgreMax::OgreMaxScene::LoadOptions loadOptions, Ogre::SceneManager *sceneManager, const QueryMask queryMask)
{
	mCollisionTool = new DFATools::DFACollisionTool(sceneManager);
	mWeaponContainers[0] = new DFAWeaponContainer(sceneManager, MAIN_MACHINE_GUN, 1470);
	mId = id;
	mNamePrefix = StringConverter::toString(id) + "_";
	mName = name;
	mQueryMask = queryMask;
	mDotSceneNode = new OgreMax::OgreMaxScene();
	OgreMax::OgreMaxScene::WhichNamePrefix prefixes = OgreMax::OgreMaxScene::OBJECT_NAME_PREFIX | OgreMax::OgreMaxScene::NODE_NAME_PREFIX;
	mDotSceneNode->SetNamePrefix(mNamePrefix, prefixes);
	mDotSceneNode->Load(sceneName, renderWindow, loadOptions, sceneManager, sceneManager->getRootSceneNode()->createChildSceneNode("AvatarMovementNode")->createChildSceneNode("AvatarNode"));

	mMovementNode = sceneManager->getSceneNode("AvatarMovementNode");
	mObjectNode = sceneManager->getSceneNode("AvatarNode");
	mCameraNode = mMovementNode->createChildSceneNode("AvatarCameraNode");

	//mMovementNode->setPosition(Vector3(500, 150, 500));

	//set camera node position
	mCameraNode->setPosition(Vector3(0, CAMERA_HEIGHT, -CAMERA_DIST));
	mCameraNode->lookAt(Vector3(0, 0, CAMERA_LOOK_AT), Node::TS_PARENT);
	mCameraNode->roll(Degree(180));

	//for better testing
	//mAvatarNode->yaw(Degree(-45));
	//mAvatarNode->scale(Vector3(1.0,1.0,1.0));

	//Entity *ent = sceneManager->createEntity("Knot", "knot.mesh");
	SceneNode *moveNode = mObjectNode->createChildSceneNode("AvatarMoveNode");
	moveNode->setPosition(Vector3(0, 0, 100));
	//moveNode->attachObject(ent);
	//moveNode->setScale(0.01, 0.01, 0.01);

	loadAnimations(sceneManager);
	loadSounds();
	initializeHUD();

	//mHeightThreadHandle = (HANDLE)_beginthreadex(NULL, 0, calculateHeightThread, this, 0, NULL);
}

Vector3 DFACore::DFAAvatar::getPosition(void)
{
	return mMovementNode->getPosition();
}

double DFACore::DFAAvatar::getMaxTargetRange(void)
{
	return MAX_TARGET_RANGE;
}

void DFACore::DFAAvatar::updateTargetList(void)
{
	//TargetList targetList = DFAData::DFAWorldData::getSingletonPtr()->updateTargetList();
	std::list<DFACore::DFAWorldObject*> worldObjects = DFAData::DFAWorldData::getSingletonPtr()->getWorldObjects();
	mTargetList.clear();
	if(worldObjects.size() > 1)
	{
		bool isInRange = false, addTarget;
		Vector3 targetDirection;
		std::list<DFACore::DFAWorldObject*>::iterator iter = worldObjects.begin();
		const Vector3 avatarPosition = getPosition();
		const Real maxTargetRange = pow(MAX_TARGET_RANGE, 2.0);
		const Real halfRadarAngle = RADAR_ANGLE/2;

		iter++;
		for(;iter != worldObjects.end(); iter++)
		{
			DFASceneObject *sceneObject = dynamic_cast<DFASceneObject*>((*iter));
			if(sceneObject)
			{
				if(!(*iter)->isDestroyed())
				{
					addTarget = false;

					if(mTargetFriendlies)
						addTarget = true;
					else
					{
						if(sceneObject->getQueryMask() == ENEMY)
							addTarget = true;
					}

					if(addTarget)
					{
						//check if the target is in the radar area
						const Vector3 objectPosition = sceneObject->getPosition();
						const Real distance = avatarPosition.squaredDistance(objectPosition);
						if(maxTargetRange >= distance)
						{
							targetDirection = objectPosition - avatarPosition;
							targetDirection.normalise();
							const Real angleBetween = mDirection.angleBetween(targetDirection).valueDegrees();
							if(angleBetween <= halfRadarAngle)
							{
								mTargetList.insert(std::make_pair(distance, *iter));
								if(*iter == mCurrentTarget)
									isInRange = true;
							}
						}
					}
				}
			}
		}

		if(mTargetSystem && !isInRange && mTargetList.size() > 0)
			mCurrentTarget = mTargetList.begin()->second;
	}
}

void DFACore::DFAAvatar::shoot(SceneManager *sceneManager)
{
	bool guidedMissile = true;
	int weaponNumber = mCurrentWeapon;

	if(mShootLeftRight && weaponNumber > 0)
		weaponNumber += 3;

	const Quaternion orientation = mMovementNode->getOrientation() * mObjectNode->getOrientation();
	Vector3 startPosition, targetPosition;
	if(weaponNumber == 0)
	{
		//set startPosition for machine gun
		const Vector3 position(0, 0, 10);
		startPosition = orientation * position;
	}
	else if(weaponNumber == 1)
	{
		//set startPosition for left innermost wing position
		const Vector3 position(-1.7, 0.3, 8);
		startPosition = orientation * position;
		mShootLeftRight = true;
	}
	else if(weaponNumber == 2)
	{
		//set startPosition for left middle wing position
		const Vector3 position(-2.5, 0.2, 8);
		startPosition = orientation * position;
		mShootLeftRight = true;
	}
	else if(weaponNumber == 3)
	{
		//set startPosition for left outermost wing position
		const Vector3 position(-3.3, 0.1, 8);
		startPosition = orientation * position;
		mShootLeftRight = true;
	}
	else if(weaponNumber == 4)
	{
		//set startPosition for right innermost wing position
		const Vector3 position(1.7, 0.3, 8);
		startPosition = orientation * position;
		mShootLeftRight = false;
	}
	else if(weaponNumber == 5)
	{
		//set startPosition for right middle wing position
		const Vector3 position(2.5, 0.2, 8);
		startPosition = orientation * position;
		mShootLeftRight = false;
	}
	else if(weaponNumber == 6)
	{
		//set startPosition for right outermost wing position
		const Vector3 position(3.3, 0.1, 8);
		startPosition = orientation * position;
		mShootLeftRight = false;
	}

	startPosition += getPosition();

	WeaponType weaponType = mWeaponContainers[weaponNumber]->getWeaponType();
	if(weaponType == ROCKET_80MM)
		guidedMissile = false;

	if(mTargetSystem && mCurrentTarget && guidedMissile)
		targetPosition = dynamic_cast<DFASceneObject*>(mCurrentTarget)->getPosition();
	else
	{
		//get position where the crosshair is pointing at
		Entity *target;
		Real distance = -1;
		Real screenX = mCrosshair->getLeft() + mCrosshair->getWidth()/2;
		Real screenY = mCrosshair->getTop() + mCrosshair->getHeight()/2;
		Camera *cam = sceneManager->getCamera("PlayCamera");
		const uint32 queryFlags = (~AVATAR) & (ENEMY | STATIONARY);
		mCollisionTool->raycastFromCamera(Root::getSingletonPtr()->getAutoCreatedWindow(),
			cam, Vector2(screenX, screenY), targetPosition, target, distance, queryFlags);
		
		if(distance < CLOSEST_TARGET_DISTANCE)
			targetPosition = (orientation * Vector3(0, 0, DEFAULT_TARGET_DISTANCE)) + getPosition();
	}

	/*/testing targetPosition
	Ogre::String name = StringConverter::toString(targetPosition.squaredLength());
	Entity *ent = sceneManager->createEntity(name, "knot.mesh");
	SceneNode *testNode = sceneManager->getRootSceneNode()->createChildSceneNode(name);
	testNode->setPosition(targetPosition);
	testNode->setScale(0.01, 0.01, 0.01);
	testNode->attachObject(ent);
	//end testing*/

	mWeaponContainers[weaponNumber]->shoot(weaponNumber, startPosition, mDirection, targetPosition);
}

void DFACore::DFAAvatar::nextWeapon(void)
{
	if(++mCurrentWeapon >= WEAPON_MAX)
		mCurrentWeapon = 0;
}

void DFACore::DFAAvatar::previousWeapon(void)
{
	if(--mCurrentWeapon < 0)
		mCurrentWeapon = WEAPON_MAX - 1;
}

void DFACore::DFAAvatar::selectWeapon(const int num)
{
	if(num >= 0 && num < WEAPON_MAX)
		mCurrentWeapon = num;
}

int DFACore::DFAAvatar::getCurrentWeapon(void)
{
	return mCurrentWeapon;
}

void DFACore::DFAAvatar::nextTarget(void)
{
	if(mTargetSystem && mTargetList.size() > 0)
	{
		TargetList::iterator iter = mTargetList.begin();
		while(iter->second != mCurrentTarget)
			iter++;

		if(++iter != mTargetList.end())
			mCurrentTarget = iter->second;
		else
			mCurrentTarget = mTargetList.begin()->second;
	}
}

void DFACore::DFAAvatar::previousTarget(void)
{
	if(mTargetSystem && mTargetList.size() > 0)
	{
		TargetList::iterator iter = mTargetList.begin();
		while(iter->second != mCurrentTarget)
			iter++;

		if(iter != mTargetList.begin())
			mCurrentTarget = (--iter)->second;
		else
			mCurrentTarget = (--mTargetList.end())->second;
	}
}

void DFACore::DFAAvatar::activateTargetSystem(const bool activate)
{
	mTargetSystem = activate;
	if(activate && mTargetList.size() > 0)
	{
		TargetList::iterator iter = mTargetList.begin();
		mCurrentTarget = iter->second;
	}
	else
	{
		mCurrentTarget = NULL;
	}
}

void DFACore::DFAAvatar::setWingPayload(const int position, DFAWeaponContainer *weaponContainer)
{
	//position goes from 1 - 3:
	//1,4 -> innermost wing position
	//2,5 -> middle wing position
	//3,6 -> outermost wing position
	if(position >= 1 && position <= 3)
	{
		DFAWeaponContainer *weaponContainer2 = new DFAWeaponContainer(*weaponContainer);
		mWeaponContainers[position] = weaponContainer;
		mWeaponContainers[position + 3] = weaponContainer2;
	}
}

bool DFACore::DFAAvatar::isDestroyed(void)
{
	return mDestroyed;
}

void DFACore::DFAAvatar::decreaseHealth(const Ogre::Real damage)
{
	mHealth -= damage;
	if(mHealth <= 0)
		mPlayDestroyedAnimation = true;
}

bool DFACore::DFAAvatar::isTargetSystemActivated(void)
{
	return mTargetSystem;
}

Ogre::String DFACore::DFAAvatar::getNodeName(void)
{
	return mObjectNode->getName();
}

Ogre::SceneNode* DFACore::DFAAvatar::getCameraNode(void)
{
	return mCameraNode;
}

void DFACore::DFAAvatar::setPosition(const Ogre::Vector3 position)
{
	mMovementNode->setPosition(position);
}

void DFACore::DFAAvatar::lookAt(const Ogre::Vector3 position)
{
	mMovementNode->lookAt(position, Node::TS_WORLD);
}

void DFACore::DFAAvatar::rotate(const Ogre::Degree degrees)
{
	mMovementNode->setOrientation(Quaternion(Degree(mAngleYaw), Vector3::UNIT_Y));
}

void DFACore::DFAAvatar::update(const Ogre::Real frameRate, Ogre::SceneManager *sceneManager)
{
	updatePosition(frameRate);
	updateAnimations(frameRate);
	if(mShoot)
		shoot(sceneManager);
	updateHUD(frameRate, sceneManager);
}

void DFACore::DFAAvatar::updatePosition(const Ogre::Real frameRate)
{
	//pitch
	if(mAccelerate)
	{
		if(mAnglePitch > -PITCH_ANGLE)
			mAnglePitch -= mPitchSpeed;
	}
	else if(mBrake)
	{
		if(mAnglePitch < PITCH_ANGLE)
			mAnglePitch += mPitchSpeed;
	}
	else
	{
		if(mAnglePitch < 0)
			mAnglePitch += mPitchSpeed/2;
		else if(mAnglePitch > 0)
			mAnglePitch -= mPitchSpeed/2;
	}

	//roll
	if(mRollRight)
	{
		if(mAngleRoll < ROLL_ANGLE)
			mAngleRoll += mRollSpeed;
	}
	else if(mRollLeft)
	{
		if(mAngleRoll > -ROLL_ANGLE)
			mAngleRoll -= mRollSpeed;
	}
	else
	{
		if(mAngleRoll > 0.1)
			mAngleRoll -= mRollSpeed/2;
		else if(mAngleRoll < -0.1)
			mAngleRoll += mRollSpeed/2;
		else
			mAngleRoll = 0;
	}

	//yaw
	if(mYawRight)
		mAngleYaw -= mYawSpeed;
	else if(mYawLeft)
		mAngleYaw += mYawSpeed;

	//climb
	mMovementNode->translate(mClimb * frameRate, Node::TS_WORLD);

	//ajust the speed
	Ogre::Real acceleration = -mAnglePitch/90 * ACCELERATION;
	if(Math::Abs(mSpeed) <= MAX_SPEED)
	{
		mSpeed += acceleration;

		if(mSpeed > 0 && mSpeed >= MAX_SPEED)
			mSpeed = MAX_SPEED;
		else if(mSpeed < 0 && Math::Abs(mSpeed) >= MAX_SPEED)
			mSpeed = -MAX_SPEED;
	}

	//adjust rotation
	if(Math::Abs(mSpeed) > TURN_SPEED_THRESHOLD)
	{
		if(mSpeed > 0)
			mAngleYaw -= TURN_SPEED * mAngleRoll/ROLL_ANGLE;
		else if(mSpeed < 0)
			mAngleYaw += TURN_SPEED * mAngleRoll/ROLL_ANGLE;
	}
	else
	{
		if(mSpeed > 0)
			mAngleYaw -= TURN_SPEED * mSpeed/TURN_SPEED_THRESHOLD * mAngleRoll/ROLL_ANGLE;
		else if(mSpeed < 0)
			mAngleYaw -= TURN_SPEED * mSpeed/TURN_SPEED_THRESHOLD * mAngleRoll/ROLL_ANGLE;
	}

	//get the direction
	Ogre::Quaternion orientation = mObjectNode->getOrientation();
	orientation.normalise();
	Vector3 forward = orientation * Ogre::Vector3(0, 0, 1.0);
	forward.normalise();
	Vector3 right = orientation * Ogre::Vector3(-1.0, 0, 0);
	right.normalise();

	Ogre::Quaternion rollPitch = Quaternion(Degree(mAngleRoll), forward) *
		Quaternion(Degree(mAnglePitch), right);
	Ogre::Quaternion yaw = Quaternion(Degree(mAngleYaw), Vector3::UNIT_Y);
	orientation = rollPitch * yaw;

	Vector3 position = ((SceneNode*)mObjectNode->getChild("AvatarMoveNode"))->getPosition();
	mDirection = orientation * position;
	mDirection.normalise();
	Vector3 planeDirection = mDirection;
	planeDirection.y = 0;

	mObjectNode->setOrientation(rollPitch);
	mMovementNode->setOrientation(yaw);

	//move to direction
	mMovementNode->translate(planeDirection * mSpeed * frameRate, Node::TS_WORLD);
}

void DFACore::DFAAvatar::updateHUD(const Ogre::Real frameRate, Ogre::SceneManager *sceneManager)
{
	//update the crosshair
	const Real crosshairCenterX = CROSSHAIR_START_X;
	const Real crosshairCenterY = (-mAnglePitch/PITCH_ANGLE * CROSSHAIR_START_Y + CROSSHAIR_OFFSET_Y);
	const Real crosshairStartX = crosshairCenterX - mCrosshair->getWidth() / 2;
	const Real crosshairStartY = crosshairCenterY - mCrosshair->getHeight() / 2;
	mCrosshair->setPosition(crosshairStartX, (crosshairStartY < 0) ? 0 : crosshairStartY);

	const Real crosshairBoxStartX = mCrosshairBox->getWidth() / 2;
	const Real crosshairBoxStartY = mCrosshairBox->getHeight() / 2;


	if(mTargetSystem && mTargetList.size() == 0)
	{
		mCurrentTarget = NULL;
	}
	else if(mTargetSystem && mTargetList.size() > 0 && !mCurrentTarget)
	{
		//find the closest target
		activateTargetSystem(true);
	}

	//update the crosshair box
	if(mTargetSystem && mCurrentTarget)
	{
		//go to selected target
		const Vector3 targetPos = dynamic_cast<DFASceneObject*>(mCurrentTarget)->getPosition();
		Camera *cam = sceneManager->getCamera("PlayCamera");
		Real screenX, screenY;
		projectPos(cam, targetPos, screenX, screenY);
		screenX = (screenX + 1) / 2;
		screenY = ((-1 * screenY) + 1) / 2;

		mCrosshairBox->setPosition(screenX - crosshairBoxStartX, screenY - crosshairBoxStartY);
	}
	else
	{
		//put crosshair box to crosshair position
		mCrosshairBox->setPosition(crosshairCenterX - crosshairBoxStartX, (crosshairCenterY < 0) ?  -crosshairBoxStartY : crosshairCenterY - crosshairBoxStartY);
	}

	//update Speed and Height stats
	Vector3 currentPos = getPosition();
	int speed = mSpeed * 3.6;
	Real terrainHeight = mCollisionTool->getTSMHeightAt(currentPos.x, currentPos.z);
	mHeight = currentPos.y - terrainHeight - 1;
	mSpeedOverlayElement->setCaption("Speed: " + StringConverter::toString(speed) + " km/h");
	mHeightOverlayElement->setCaption("Height: " + StringConverter::toString((int)mHeight) + " m");

	//update Weapon stats
	if(mCurrentTarget)
		mTargetNameOverlayElement->setCaption("Target: " + mCurrentTarget->getName());
	else
		mTargetNameOverlayElement->setCaption("Target: ");

	mWeaponNameOverlayElement->setCaption("Weapon: " + mWeaponContainers[mCurrentWeapon]->getWeaponName());
	if(mCurrentWeapon == 0)
		mWeaponAmmoOverlayElement->setCaption("Ammo: " + StringConverter::toString(mWeaponContainers[mCurrentWeapon]->getAmmunition()));
	else
	{
		const int totalAmmunition = mWeaponContainers[mCurrentWeapon]->getAmmunition() + mWeaponContainers[mCurrentWeapon + 3]->getAmmunition();
		mWeaponAmmoOverlayElement->setCaption("Ammo: " + StringConverter::toString(totalAmmunition));
	}
}

void DFACore::DFAAvatar::calculateHeight(void)
{
	while(true)
	{
		if(mAnimationStates.size() > 0)
		{
			std::list<AnimationState*>::iterator iter = mAnimationStates.begin();
			for(;iter != mAnimationStates.end(); iter++)
			{
				(*iter)->addTime(1.0/60.0);
			}
		}

		Sleep(50);
	}
}

unsigned __stdcall DFACore::DFAAvatar::calculateHeightThread(void *param)
{
	((DFAAvatar*)param)->calculateHeight();

	return 0;
}

OgreMax::OgreMaxScene::ObjectExtraDataMap& DFACore::DFAAvatar::getObjectExtraDataMap(void)
{
	return mDotSceneNode->GetAllObjectExtraData();
}

void DFACore::DFAAvatar::shoot(bool pressed, Ogre::SceneManager *sceneManager)
{
	if(pressed)
	{
		DFACore::WeaponType weaponType = mWeaponContainers[mCurrentWeapon]->getWeaponType();
		if(weaponType == DFACore::MAIN_MACHINE_GUN)
			mShoot = true;
		else
			shoot(sceneManager);
	}
	else
		mShoot = pressed;
}

void DFACore::DFAAvatar::accelerate(bool pressed)
{
	if(pressed)
	{
		mAccelerate = true;
		mBrake = false;
	}
	else
	{
		mAccelerate = false;
		mBrake = false;
	}
}

void DFACore::DFAAvatar::brake(bool pressed)
{
	if(pressed)
	{
		mBrake = true;
		mAccelerate = false;
	}
	else
	{
		mBrake = false;
		mAccelerate = false;
	}
}

void DFACore::DFAAvatar::moveLeft(bool pressed)
{
	if(pressed)
	{
		mRollLeft = true;
		mRollRight = false;
	}
	else
	{
		mRollRight = false;
		mRollLeft = false;
	}
}

void DFACore::DFAAvatar::moveRight(bool pressed)
{
	if(pressed)
	{
		mRollRight = true;
		mRollLeft = false;
	}
	else
	{
		mRollRight = false;
		mRollLeft = false;
	}
}

void DFACore::DFAAvatar::rotateLeft(bool pressed)
{
	if(pressed)
	{
		mYawLeft = true;
		mYawRight = false;
	}
	else
	{
		mYawRight = false;
		mYawLeft = false;
	}
}

void DFACore::DFAAvatar::rotateRight(bool pressed)
{
	if(pressed)
	{
		mYawRight = true;
		mYawLeft = false;
	}
	else
	{
		mYawRight = false;
		mYawLeft = false;
	}
}

void DFACore::DFAAvatar::moveUp(bool pressed)
{
	if(pressed)
		mClimb.y = MAX_CLIMB_SPEED;
	else
		mClimb.y = 0;
}

void DFACore::DFAAvatar::moveDown(bool pressed)
{
	if(pressed)
		mClimb.y = -MAX_CLIMB_SPEED;
	else
		mClimb.y = 0;
}

Ogre::String DFACore::DFAAvatar::getName(void)
{
	return mName;
}

DFACore::QueryMask DFACore::DFAAvatar::getQueryMask(void)
{
	return mQueryMask;
}

Ogre::Quaternion DFACore::DFAAvatar::getOrientation(void)
{
	return mMovementNode->getOrientation() * mObjectNode->getOrientation();
}

Ogre::Real DFACore::DFAAvatar::getRadarAngle(void)
{
	return RADAR_ANGLE;
}

void DFACore::DFAAvatar::targetFriendlies(const bool targetFriendlies)
{
	mTargetFriendlies = targetFriendlies;
}

bool DFACore::DFAAvatar::projectPos(Ogre::Camera *cam, const Ogre::Vector3 &pos, Ogre::Real &x, Ogre::Real &y)
{
	Vector3 eyeSpacePos = cam->getViewMatrix(true) * pos;
	// z < 0 means in front of cam
	if (eyeSpacePos.z < 0)
	{
		// calculate projected pos
		Vector3 screenSpacePos = cam->getProjectionMatrix() * eyeSpacePos;
		if(screenSpacePos.x < -1)
			x = -1;
		else if(screenSpacePos.x > 1)
			x = 1;
		else
			x = screenSpacePos.x;

		if(screenSpacePos.y < -1)
			y = -1;
		else if(screenSpacePos.y > 1)
			y = 1;
		else
			y = screenSpacePos.y;

		return true;
	}
	else
	{
		x = (-eyeSpacePos.x > 0) ? -1 : 1;
		y = (-eyeSpacePos.y > 0) ? -1 : 1;

		return false;
	}
}


DFACore::DFAAvatar* DFACore::DFAAvatar::getSingletonPtr(void)
{
	if(!mAvatar)
	{
		mAvatar = new DFACore::DFAAvatar();
	}

	return mAvatar;
	
	//return ms_Singleton;
}