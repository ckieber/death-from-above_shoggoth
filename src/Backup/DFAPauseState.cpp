#include <Ogre.h>

#include "DFAPauseState.h"

using namespace Ogre;

DFACore::DFAPauseState DFACore::DFAPauseState::mPauseState;

void DFACore::DFAPauseState::enter()
{
	mRoot = Root::getSingletonPtr();
	mKeyboard = DFAInput::DFAInputManager::getSingletonPtr()->getKeyboard();
	mMouse = DFAInput::DFAInputManager::getSingletonPtr()->getMouse();

	mViewport = mRoot->getAutoCreatedWindow()->getViewport(0);
	mViewport->setBackgroundColour(ColourValue(0.0, 0.0, 1.0));
}

void DFACore::DFAPauseState::exit()
{
}

void DFACore::DFAPauseState::pause()
{
}

void DFACore::DFAPauseState::resume()
{
}

bool DFACore::DFAPauseState::keyClicked(const OIS::KeyEvent &e)
{
	return true;
}

bool DFACore::DFAPauseState::keyPressed(const OIS::KeyEvent &e)
{
	switch(e.key)
	{
		case OIS::KC_P:
			popState();
			break;
		default:
			break;
	}

	return true;
}

bool DFACore::DFAPauseState::keyReleased(const OIS::KeyEvent &e)
{
	return true;
}

bool DFACore::DFAPauseState::mouseMoved(const OIS::MouseEvent &e)
{
	return true;
}

bool DFACore::DFAPauseState::mousePressed(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
	return true;
}

bool DFACore::DFAPauseState::mouseReleased(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
	return true;
}

bool DFACore::DFAPauseState::frameStarted(const FrameEvent &evt)
{
	DFAInput::DFAInputManager::getSingletonPtr()->capture();

	return true;
}

bool DFACore::DFAPauseState::frameEnded(const FrameEvent &evt)
{
	return true;
}