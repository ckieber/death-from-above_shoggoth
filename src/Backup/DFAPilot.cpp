#include <sstream>

#include "DFAPilot.h"

DFAData::DFAPilot::DFAPilot(void)
{
	mName = "";
	mRank = Junior_Lieutenant;
	mScore = 0;
	mStatus = ACTIVE;
	mCompleteMissionsDay = 0;
	mCompleteMissionsNight = 0;
	mIncompleteMissionsDay = 0;
	mIncompleteMissionsNight = 0;
	mKills = 0;
}

DFAData::DFAPilot::DFAPilot(const CEGUI::String name)
{
	mName = name;
	mRank = Junior_Lieutenant;
	mScore = 0;
	mStatus = ACTIVE;
	mCompleteMissionsDay = 0;
	mCompleteMissionsNight = 0;
	mIncompleteMissionsDay = 0;
	mIncompleteMissionsNight = 0;
	mKills = 0;
}

void DFAData::DFAPilot::resetPilot(void)
{
	mRank = Junior_Lieutenant;
	mScore = 0;
	mStatus = ACTIVE;
	mCompleteMissionsDay = 0;
	mCompleteMissionsNight = 0;
	mIncompleteMissionsDay = 0;
	mIncompleteMissionsNight = 0;
	mKills = 0;
}

const CEGUI::String DFAData::DFAPilot::getName(void) const
{
	return mName;
}

void DFAData::DFAPilot::setName(const CEGUI::String name)
{
	mName = name;
}

const CEGUI::String DFAData::DFAPilot::getStatus(void) const
{
	switch(mStatus)
	{
		case ACTIVE:
			return "Active";
		case INJURED:
			return "Injured";
		case DEAD:
			return "Dead";
		default:
			return "";
	}
}

void DFAData::DFAPilot::setStatus(const PilotStatus status)
{
	mStatus = status;
}

void DFAData::DFAPilot::setStatus(const CEGUI::String status)
{
	if(status == "Active")
			mStatus = ACTIVE;
	else if(status == "Injured")
			mStatus = INJURED;
	else if(status == "Dead")
			mStatus = DEAD;
}

const CEGUI::String DFAData::DFAPilot::getRank(void) const
{
	switch(mRank)
	{
		case Junior_Lieutenant:
			return "Junior Lieutenant";
		case Lieutenant:
			return "Lieutenant";
		case Senior_Lieutenant:
			return "Senior Lieutenant";
		case Captain:
			return "Captain";
		case Major:
			return "Major";
		case Lieutenant_Colonel:
			return "Lieutenant Colonel";
		case Colonel:
			return "Colonel";
		case Major_General:
			return "Major General";
		case Lieutenant_General:
			return "Lieutenant General";
		case Colonel_General:
			return "Colonel General";
		case General_Of_The_Army:
			return "General Of The Army";
		default:
			return "";
	}
}
void DFAData::DFAPilot::setRank(const PilotRank rank)
{
	mRank = rank;
}

void DFAData::DFAPilot::setRank(const CEGUI::String rank)
{
	if(rank == "Junior Lieutenant")
			mRank = Junior_Lieutenant;
	else if(rank == "Lieutenant")
			mRank = Lieutenant;
	else if(rank == "Senior Lieutenant")
			mRank = Senior_Lieutenant;
	else if(rank == "Captain")
			mRank = Captain;
	else if(rank == "Major")
			mRank = Major;
	else if(rank == "Lieutenant Colonel")
			mRank = Lieutenant_Colonel;
	else if(rank == "Colonel")
			mRank = Colonel;
	else if(rank == "Major General")
			mRank = Major_General;
	else if(rank == "Lieutenant General")
			mRank = Lieutenant_General;
	else if(rank == "Colonel General")
			mRank = Colonel_General;
	else if(rank == "General Of The Army")
			mRank = General_Of_The_Army;
}

void DFAData::DFAPilot::promote(void)
{
	switch(mRank)
	{
		case Junior_Lieutenant:
			mRank = Lieutenant;
			break;
		case Lieutenant:
			mRank = Senior_Lieutenant;
			break;
		case Senior_Lieutenant:
			mRank = Captain;
			break;
		case Captain:
			mRank = Major;
			break;
		case Major:
			mRank = Lieutenant_Colonel;
			break;
		case Lieutenant_Colonel:
			mRank = Colonel;
			break;
		case Colonel:
			mRank = Major_General;
			break;
		case Major_General:
			mRank = Lieutenant_General;
			break;
		case Lieutenant_General:
			mRank = Colonel_General;
			break;
		case Colonel_General:
			mRank = General_Of_The_Army;
			break;
	}
}
void DFAData::DFAPilot::demote(void)
{
	switch(mRank)
	{
		case Lieutenant:
			mRank = Junior_Lieutenant;
			break;
		case Senior_Lieutenant:
			mRank = Lieutenant;
			break;
		case Captain:
			mRank = Senior_Lieutenant;
			break;
		case Major:
			mRank = Captain;
			break;
		case Lieutenant_Colonel:
			mRank = Major;
			break;
		case Colonel:
			mRank = Lieutenant_Colonel;
			break;
		case Major_General:
			mRank = Colonel;
			break;
		case Lieutenant_General:
			mRank = Major_General;
			break;
		case Colonel_General:
			mRank = Lieutenant_General;
			break;
		case General_Of_The_Army:
			mRank = Colonel_General;
			break;
	}
}

const int DFAData::DFAPilot::getScore(void) const
{
	return mScore;
}

void DFAData::DFAPilot::setScore(const int score)
{
	mScore = score;
}

const CEGUI::String DFAData::DFAPilot::getPlayedMissions(void) const
{
	return intToString(mCompleteMissionsDay + mCompleteMissionsNight + mIncompleteMissionsDay + mIncompleteMissionsNight);
}

const CEGUI::String DFAData::DFAPilot::getCompMissionsTotal(void) const
{
	return intToString(mCompleteMissionsDay + mCompleteMissionsNight);
}

const CEGUI::String DFAData::DFAPilot::getCompMissionsDay(void) const
{
	return intToString(mCompleteMissionsDay);
}

void DFAData::DFAPilot::setCompMissionsDay(const int completeMissionsDay)
{
	mCompleteMissionsDay = completeMissionsDay;
}

const CEGUI::String DFAData::DFAPilot::getCompMissionsNight(void) const
{
	return intToString(mCompleteMissionsNight);
}

void DFAData::DFAPilot::setCompMissionsNight(const int completeMissionsNight)
{
	mCompleteMissionsNight = completeMissionsNight;
}

const CEGUI::String DFAData::DFAPilot::getIncompMissionsTotal(void) const
{
	return intToString(mIncompleteMissionsDay + mIncompleteMissionsNight);
}

const CEGUI::String DFAData::DFAPilot::getIncompMissionsDay(void) const
{
	return intToString(mIncompleteMissionsDay);
}

void DFAData::DFAPilot::setIncompMissionsDay(const int incompleteMissionsDay)
{
	mIncompleteMissionsDay = incompleteMissionsDay;
}

const CEGUI::String DFAData::DFAPilot::getIncompMissionsNight(void) const
{
	return intToString(mIncompleteMissionsNight);
}

void DFAData::DFAPilot::setIncompMissionsNight(const int incompleteMissionsNight)
{
	mIncompleteMissionsNight = incompleteMissionsNight;
}

const CEGUI::String DFAData::DFAPilot::getKills(void) const
{
	return intToString(mKills);
}

void DFAData::DFAPilot::setKills(const int kills)
{
	mKills = kills;
}

void DFAData::DFAPilot::addKills(const int kills)
{
	mKills += kills;
}

void DFAData::DFAPilot::increaseCompMissionsDay(void)
{
	mCompleteMissionsDay++;
}

void DFAData::DFAPilot::increaseCompMissionsNight(void)
{
	mCompleteMissionsNight++;
}

void DFAData::DFAPilot::increaseIncompMissionsDay(void)
{
	mIncompleteMissionsDay++;
}

void DFAData::DFAPilot::increaseIncompMissionsNight(void)
{
	mIncompleteMissionsNight++;
}



DFAData::DFAPilot& DFAData::DFAPilot::operator =(const DFAData::DFAPilot &pilot)
{
	mName = pilot.getName();
	return *this;
}

int DFAData::DFAPilot::operator ==(const DFAData::DFAPilot &pilot)
{
	if(mName == pilot.getName())
		return 1;
	return 0;
}

int DFAData::DFAPilot::operator <(const DFAData::DFAPilot &pilot)
{
	if(mName < pilot.getName())
		return 1;
	return 0;
}

const CEGUI::String DFAData::DFAPilot::intToString(int integer) const
{
	std::stringstream s;
	s << integer;
	return (CEGUI::String)s.str();
}