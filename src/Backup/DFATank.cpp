#include "DFATank.h"
#include "OgreOggSound.h"

#define CAMERA_DIST 215
#define CAMERA_HEIGHT 75

#define	WEAPON_MAX 2

using namespace Ogre;

DFAAI::DFATank::DFATank()
{
	mCurrentWeapon = 0;
}

DFAAI::DFATank::~DFATank()
{
	if(mTankScene)
		mTankScene->Destroy();

	if(mCameraNode)
		delete mCameraNode;
	if(mTankNode)
		delete mTankNode;
	if(mMovementNode)
		delete mMovementNode;
}

void DFAAI::DFATank::loadAnimations(Ogre::SceneManager *sceneManager)
{
	
}



void DFAAI::DFATank::loadSounds()
{
	
}

void DFAAI::DFATank::updateAnimations(const Ogre::FrameEvent &evt)
{
	
}

void DFAAI::DFATank::createObject(const Ogre::String name, const int id, const Ogre::String &sceneName, Ogre::RenderWindow *renderWindow,
			OgreMax::OgreMaxScene::LoadOptions loadOptions, Ogre::SceneManager *sceneManager)
{
	mId = id;
	mName = name;
	mTankScene = new OgreMax::OgreMaxScene();
	mTankScene->Load(id, sceneName, renderWindow, loadOptions, sceneManager, sceneManager->getRootSceneNode()->createChildSceneNode("TankMovementNode_" + id)->createChildSceneNode("TankNode_" + id));
	
	mMovementNode = sceneManager->getSceneNode("TankMovementNode_" + id);
	mTankNode = sceneManager->getSceneNode("TankNode_" + id);
	mCameraNode = mMovementNode->createChildSceneNode("TankCameraNode_" + id);

	//mMovementNode->setPosition(Vector3(500, 150, 500));

	//set camera node position
	mCameraNode->setPosition(Vector3(0, CAMERA_HEIGHT, -CAMERA_DIST));
	mCameraNode->lookAt(Vector3(0, 0, 500), Node::TS_PARENT);
	mCameraNode->roll(Degree(180));

	//for better testing
	//mAvatarNode->yaw(Degree(-45));
	mTankNode->scale(Vector3(0.08, 0.08, 0.08));

	//Entity *ent = sceneManager->createEntity("Knot", "knot.mesh");
	SceneNode *moveNode = mTankNode->createChildSceneNode("TankMoveNode_" + id);
	moveNode->setPosition(Vector3(0, 0, 100));
	//moveNode->attachObject(ent);
	//moveNode->setScale(0.01, 0.01, 0.01);

	loadAnimations(sceneManager);
	loadSounds();
}

MovableObject* DFAAI::DFATank::getTextDisplayEntity(Ogre::SceneManager *sceneMgr)
{
	return sceneMgr->getEntity("Hull_0");
}

Vector3 DFAAI::DFATank::getPosition(void)
{
	return mMovementNode->getPosition();
}

void DFAAI::DFATank::shoot(void)
{

}

void DFAAI::DFATank::nextWeapon(void)
{
	if(++mCurrentWeapon >= WEAPON_MAX)
		mCurrentWeapon = 0;
}

void DFAAI::DFATank::previousWeapon(void)
{
	if(--mCurrentWeapon < 0)
		mCurrentWeapon = WEAPON_MAX;
}

void DFAAI::DFATank::selectWeapon(const int num)
{
	if(num >= 0 && num < WEAPON_MAX)
		mCurrentWeapon = num;
}

int DFAAI::DFATank::getCurrentWeapon(void)
{
	return mCurrentWeapon;
}

Ogre::String DFAAI::DFATank::getNodeName(void)
{
	return mTankNode->getName();
}

Ogre::SceneNode* DFAAI::DFATank::getCameraNode(void)
{
	return mCameraNode;
}

void DFAAI::DFATank::setStartingPosition(const Ogre::Vector3 position)
{
	mMovementNode->setPosition(position);
}

void DFAAI::DFATank::updatePosition(const FrameEvent &evt)
{
	
}

void DFAAI::DFATank::accelerate(bool pressed)
{
	
}

void DFAAI::DFATank::brake(bool pressed)
{
	
}

void DFAAI::DFATank::moveLeft(bool pressed)
{
	
}

void DFAAI::DFATank::moveRight(bool pressed)
{
	
}

void DFAAI::DFATank::rotateLeft(bool pressed)
{
	
}

void DFAAI::DFATank::rotateRight(bool pressed)
{

}

void DFAAI::DFATank::moveUp(bool pressed)
{
	
}

void DFAAI::DFATank::moveDown(bool pressed)
{
	
}

Ogre::String DFAAI::DFATank::getName(void)
{
	return mName;
}
