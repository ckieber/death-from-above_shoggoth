#include "DFALevelCreator.h"
#include "DFAAvatar.h"
#include "DFATank.h"
#include "DFAObjectTextDisplay.h"

using namespace Ogre;

DFAData::DFALevelCreator *DFAData::DFALevelCreator::mLevelCreator = 0;

DFAData::DFALevelCreator::~DFALevelCreator()
{
	
}

std::list<DFACore::DFAWorldObject*> DFAData::DFALevelCreator::createLevel(const int levelNumber, Ogre::SceneManager *sceneMgr, Ogre::RenderWindow *renderWindow,
												OgreOggSound::OgreOggSoundManager *soundManager, Ogre::Camera *camera)
{
	std::list<DFACore::DFAWorldObject*> worldObjects;

	sceneMgr->setAmbientLight(ColourValue(0.25, 0.25, 0.25));
	
	//creating terrain
	OgreMax::OgreMaxScene *desertScene = new OgreMax::OgreMaxScene();
	desertScene->Load(0, "Desert.scene", renderWindow, OgreMax::OgreMaxScene::NO_OPTIONS, sceneMgr, sceneMgr->getRootSceneNode()->createChildSceneNode("DesertNode"));

	Ogre::SceneNode *desertNode = sceneMgr->getSceneNode("DesertNode");
	desertNode->scale(Vector3(500, 500, 500));
	desertNode->setPosition(Vector3(0, -500, 0));

	//add hangars
	OgreMax::OgreMaxScene *hangarScene = new OgreMax::OgreMaxScene();
	hangarScene->Load(0, "Hangar.scene", renderWindow, OgreMax::OgreMaxScene::NO_OPTIONS, sceneMgr, sceneMgr->getRootSceneNode()->createChildSceneNode("HangarNode_0"));
	Ogre::SceneNode *hangarNode = sceneMgr->getSceneNode("HangarNode_0");
	hangarNode->setPosition(Vector3(1000, 50, -4900));
	hangarNode->setScale(Vector3(0.5, 0.5, 0.5));

	hangarScene = new OgreMax::OgreMaxScene();
	hangarScene->Load(1, "Hangar.scene", renderWindow, OgreMax::OgreMaxScene::NO_OPTIONS, sceneMgr, sceneMgr->getRootSceneNode()->createChildSceneNode("HangarNode_1"));
	hangarNode = sceneMgr->getSceneNode("HangarNode_1");
	hangarNode->setPosition(Vector3(800, 50, -4900));
	hangarNode->setScale(Vector3(0.5, 0.5, 0.5));

	//create the sky with sun
	sceneMgr->setSkyDome(true, "Sky", 2, 4, 10000);
	Light *sun = sceneMgr->createLight("SunLight");
	sun->setType(Light::LT_DIRECTIONAL);
	sun->setDirection(Vector3(0, -1, -1));
	sun->setDiffuseColour(ColourValue::White);
	sun->setSpecularColour(ColourValue::White);
	

	/*
	SceneNode *node = mSceneMgr->getRootSceneNode()->createChildSceneNode("camNode1", Vector3(0,0,500));
	node->lookAt(Vector3::ZERO, Node::TS_WORLD);
	node->attachObject(mCamera);
	node->attachObject(mSoundManager->getListener());
	*/

	//add avatar
	DFACore::DFAAvatar *avatar = DFACore::DFAAvatar::getSingletonPtr();
	avatar->createObject("Avatar", 0, "Mi24.scene", renderWindow, OgreMax::OgreMaxScene::NO_OPTIONS, sceneMgr);
	avatar->setStartingPosition(Vector3(900, 80, -4970));
	avatar->lookAt(Vector3::ZERO);
	worldObjects.push_back(avatar);

	//add camera
	SceneNode *camNode = avatar->getCameraNode();
	camNode->attachObject(camera);
	camNode->attachObject(soundManager->getListener());


	//add tanks
	DFAAI::DFATank *abramsTank = new DFAAI::DFATank();
	abramsTank->createObject("M1A2 Abrams", 0, "Abrams2.scene", renderWindow, OgreMax::OgreMaxScene::NO_OPTIONS, sceneMgr);
	abramsTank->setStartingPosition(Vector3(0, 100, 100));
	worldObjects.push_back(abramsTank);

	abramsTank = new DFAAI::DFATank();
	abramsTank->createObject("M1A2 Abrams", 1, "Abrams2.scene", renderWindow, OgreMax::OgreMaxScene::NO_OPTIONS, sceneMgr);
	abramsTank->setStartingPosition(Vector3(0, 100, 200));
	worldObjects.push_back(abramsTank);






	//just random stuff
	/*Light *light = sceneMgr->createLight("Light1");
	light->setType(Light::LT_POINT);
	light->setPosition(Vector3(250, 150, 250));
	light->setDiffuseColour(ColourValue::White);
	light->setSpecularColour(ColourValue::White);
	*/

	//just temporary -> need to change
	//std::list<DFACore::DFAWorldObject*> temp;
	return worldObjects;
}

DFAData::DFALevelCreator* DFAData::DFALevelCreator::getSingletonPtr(void)
{
	if(!mLevelCreator)
	{
		mLevelCreator = new DFAData::DFALevelCreator();
	}

	return mLevelCreator;
	
	//return ms_Singleton;
}