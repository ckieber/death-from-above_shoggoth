#include <Ogre.h>

#include "DFAPlayState.h"
#include "DFALevelCreator.h"
#include "DFADotSceneLoader.h"
#include "DFATank.h"
#include "DFAObjectTextDisplay.h"

using namespace Ogre;

DFACore::DFAPlayState DFACore::DFAPlayState::mPlayState;

void DFACore::DFAPlayState::enter()
{
	mRoot = Root::getSingletonPtr();
	mKeyboard = DFAInput::DFAInputManager::getSingletonPtr()->getKeyboard();
	mMouse = DFAInput::DFAInputManager::getSingletonPtr()->getMouse();

	//mSceneMgr = mRoot->createSceneManager(SceneType::ST_GENERIC);
    mSceneMgr = mRoot->createSceneManager("OctreeSceneManager", "MainSceneManager");

	mCamera = mSceneMgr->createCamera("PlayCamera");
	mViewport = mRoot->getAutoCreatedWindow()->addViewport(mCamera);
	mCamera->setAspectRatio(Real(mViewport->getActualWidth())/Real(mViewport->getActualHeight()));
	//mCamera->setPosition(Vector3(0,0,500));
	//mCamera->lookAt(Vector3::ZERO);
	//mViewport->setBackgroundColour(ColourValue(0.0, 0.0, 1.0));

	mSoundManager = OgreOggSound::OgreOggSoundManager::getSingletonPtr();
	//mSoundManager->init();
	mSoundManager->setDistanceModel(AL_LINEAR_DISTANCE);

	createScene();

	mDoorPressed = false;
	mGearPressed = false;
	mExitGame = false;
}

void DFACore::DFAPlayState::exit()
{
	if(mSoundManager)
		delete mSoundManager;

	mSceneMgr->clearScene();
	mSceneMgr->destroyAllCameras();
	mRoot->getAutoCreatedWindow()->removeAllViewports();
}

void DFACore::DFAPlayState::pause()
{
}

void DFACore::DFAPlayState::resume()
{
}

bool DFACore::DFAPlayState::keyClicked(const OIS::KeyEvent &e)
{
	return true;
}

bool DFACore::DFAPlayState::keyPressed(const OIS::KeyEvent &e)
{
	switch(e.key)
	{
		case OIS::KC_ESCAPE:
			mExitGame = true;
			break;
		case OIS::KC_UP:
		case OIS::KC_W:
			mAvatar->accelerate(true);
			break;
		case OIS::KC_DOWN:
		case OIS::KC_S:
			mAvatar->brake(true);
			break;
		case OIS::KC_LEFT:
		case OIS::KC_A:
			mAvatar->moveLeft(true);
			break;
		case OIS::KC_RIGHT:
		case OIS::KC_D:
			mAvatar->moveRight(true);
			break;
		case OIS::KC_PGDOWN:
		case OIS::KC_E:
			mAvatar->moveDown(true);
			break;
		case OIS::KC_PGUP:
		case OIS::KC_Q:
			mAvatar->moveUp(true);
			break;
		case OIS::KC_X:
			mAvatar->rotateRight(true);
			break;
		case OIS::KC_Z:
			mAvatar->rotateLeft(true);
			break;
		case OIS::KC_U:
			if(!mDoorPressed)
			{
				mAvatar->doorsOpen(!mAvatar->isDoorOpen());
				mDoorPressed = true;
			}
			break;
		case OIS::KC_G:
			if(!mGearPressed)
			{
				mAvatar->gearUp(!mAvatar->isGearUp());
				mGearPressed = true;
			}
			break;
		default:
			break;
	}
	return !mExitGame;
}

bool DFACore::DFAPlayState::keyReleased(const OIS::KeyEvent &e)
{
	
	switch (e.key)
	{
		case OIS::KC_UP:
		case OIS::KC_W:
			mAvatar->accelerate(false);
			break;
		case OIS::KC_DOWN:
		case OIS::KC_S:
			mAvatar->brake(false);
			break;
		case OIS::KC_LEFT:
		case OIS::KC_A:
			mAvatar->moveLeft(false);
			break;
		case OIS::KC_RIGHT:
		case OIS::KC_D:
			mAvatar->moveRight(false);
			break;
		case OIS::KC_PGDOWN:
		case OIS::KC_E:
			mAvatar->moveDown(false);
			break;
		case OIS::KC_PGUP:
		case OIS::KC_Q:
			mAvatar->moveUp(false);
			break;
		case OIS::KC_X:
			mAvatar->rotateRight(false);
			break;
		case OIS::KC_Z:
			mAvatar->rotateLeft(false);
			break;
		case OIS::KC_U:
			mDoorPressed = false;
			break;
		case OIS::KC_G:
			mGearPressed = false;
			break;
		default:
			break;
		}
		

	return true;
}

bool DFACore::DFAPlayState::mouseMoved(const OIS::MouseEvent &e)
{
	return true;
}

bool DFACore::DFAPlayState::mousePressed(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
	/*
	Light *light = mSceneMgr->getLight("Light1");
	switch(id)
	{
		case OIS::MB_Left:
			light->setVisible(!light->isVisible());
			break;
		default:
			break;
	}
	*/
	return true;
}

bool DFACore::DFAPlayState::mouseReleased(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
	return true;
}

bool DFACore::DFAPlayState::frameStarted(const FrameEvent &evt)
{
	DFAInput::DFAInputManager::getSingletonPtr()->capture();

	mSoundManager->update(evt.timeSinceLastFrame);

	//update position and animation for all worldObjects
	if(mWorldObjects.size() > 0)
	{
		std::list<DFAWorldObject*>::iterator iter = mWorldObjects.begin();
		for(;iter != mWorldObjects.end(); iter++)
		{
			(*iter)->updatePosition(evt);
			(*iter)->updateAnimations(evt);
		}
	}

	//mAvatar->setTargetList(mWorldData->updateTargetList(evt, mWorldObjects, mSceneMgr));
	//mText->update();

	return true;
}

bool DFACore::DFAPlayState::frameEnded(const FrameEvent &evt)
{
	return !mExitGame;
}

void DFACore::DFAPlayState::createScene()
{
	DFAData::DFALevelCreator *levelCreator = DFAData::DFALevelCreator::getSingletonPtr();
	mWorldObjects = levelCreator->createLevel(0, mSceneMgr, mRoot->getAutoCreatedWindow(), mSoundManager, mCamera);
	mAvatar = (DFAAvatar*) mWorldObjects.front();

	mWorldData = DFAData::DFAWorldData::getSingletonPtr();

	//std::list<DFAWorldObject*>::iterator iter = mWorldObjects.begin();
	//iter++;

	//mText = new DFATools::ObjectTextDisplay(((DFAAI::DFATank*)(*iter))->getTextDisplayEntity(mSceneMgr),
		//mCamera);
	//mText->enable(true);
    //mText->setText("Target");
	
	/*
	//mSceneMgr->setWorldGeometry("terrain.cfg");
	
	SceneNode *node = mSceneMgr->getRootSceneNode()->createChildSceneNode("camNode1", Vector3(0,0,500));
	node->lookAt(Vector3::ZERO, Node::TS_WORLD);
	node->attachObject(mCamera);
	node->attachObject(mSoundManager->getListener());
	

	//mSceneMgr->setAmbientLight(ColourValue(0.25, 0.25, 0.25));

	mAvatar = DFAAvatar::getSingletonPtr();
	mAvatar->createObject(0, "Mi24.scene", mRoot->getAutoCreatedWindow(), OgreMax::OgreMaxScene::NO_OPTIONS, mSceneMgr);
	/*mWorldObjects.push_back(mAvatar);

	
	//add camera
	SceneNode *camNode = mAvatar->getCameraNode();
	camNode->attachObject(mCamera);
	camNode->attachObject(mSoundManager->getListener());
	

	


	//just random stuff
	Light *light = mSceneMgr->createLight("Light1");
	light->setType(Light::LT_POINT);
	light->setPosition(Vector3(250, 150, 250));
	light->setDiffuseColour(ColourValue::White);
	light->setSpecularColour(ColourValue::White);
	*/
}