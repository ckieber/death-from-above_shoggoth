#include "DFAAvatar.h"
#include "OgreOggSound.h"
#include <sstream>

#define PITCH_ANGLE 15
#define ROLL_ANGLE 20
#define PITCH_SPEED 0.25
#define ROLL_SPEED 0.35
#define YAW_SPEED 0.15
#define MAX_SPEED 250
#define MAX_CLIMB_SPEED 50
#define TURN_SPEED_THRESHOLD 50
#define TURN_SPEED 0.35

#define CAMERA_DIST 215
#define CAMERA_HEIGHT 75

#define	WEAPON_MAX 3
#define MAX_TARGET_RANGE 300

using namespace Ogre;

DFACore::DFAAvatar *DFACore::DFAAvatar::mAvatar = 0;
//template<> DFACore::DFAAvatar *Singleton<DFACore::DFAAvatar>::ms_Singleton = 0;

DFACore::DFAAvatar::DFAAvatar()
{
	mSpeed = 0;
	mClimb = Vector3::ZERO;
	mDirection = Vector3::ZERO;

	mAnglePitch = 0;
	mAngleRoll = 0;
	mAngleYaw = 0;

	mPitchSpeed = PITCH_SPEED;
	mBrake = false;
	mAccelerate = false;

	mRollSpeed = ROLL_SPEED;
	mRollLeft = false;
	mRollRight = false;

	mYawSpeed = YAW_SPEED;
	mYawLeft = false;
	mYawRight = false;

	mDoorsOpen = true;
	mGearUp = false;

	mCurrentWeapon = 0;
}

DFACore::DFAAvatar::~DFAAvatar()
{
	if(mAvatarScene)
		mAvatarScene->Destroy();

	if(mCameraNode)
		delete mCameraNode;
	if(mAvatarNode)
		delete mAvatarNode;
	if(mMovementNode)
		delete mMovementNode;
}

void DFACore::DFAAvatar::loadAnimations(const int id, Ogre::SceneManager *sceneManager)
{
	AnimationState *state = mAvatarScene->GetAnimationState("mainConst");
	state->setEnabled(true);
	state->setLoop(true);
	mAnimationStates.push_back(state);

	std::stringstream postId;
	String mainRotorString = "main_rotor_";
	postId << mainRotorString << id;
	Entity *mainRotor = sceneManager->getEntity(postId.str());
	state = mainRotor->getAnimationState("mainLift");
	state->setEnabled(true);
	state->setLoop(false);
	mAnimationStates.push_back(state);

	state = mAvatarScene->GetAnimationState("tailConst");
	state->setEnabled(true);
	state->setLoop(true);
	mAnimationStates.push_back(state);

	//door closing animation
	state = mAvatarScene->GetAnimationState("doorLeftLowClose");
	mAnimationStates.push_back(state);
	state = mAvatarScene->GetAnimationState("doorLeftHighClose");
	mAnimationStates.push_back(state);
	state = mAvatarScene->GetAnimationState("doorRightLowClose");
	mAnimationStates.push_back(state);
	state = mAvatarScene->GetAnimationState("doorRightHighClose");
	mAnimationStates.push_back(state);

	//door opening animation
	state = mAvatarScene->GetAnimationState("doorLeftLowOpen");
	mAnimationStates.push_back(state);
	state = mAvatarScene->GetAnimationState("doorLeftHighOpen");
	mAnimationStates.push_back(state);
	state = mAvatarScene->GetAnimationState("doorRightLowOpen");
	mAnimationStates.push_back(state);
	state = mAvatarScene->GetAnimationState("doorRightHighOpen");
	mAnimationStates.push_back(state);

	//gear up animations
	state = mAvatarScene->GetAnimationState("rightWheelUp");
	mAnimationStates.push_back(state);
	state = mAvatarScene->GetAnimationState("rightWheelCoverUp");
	mAnimationStates.push_back(state);
	state = mAvatarScene->GetAnimationState("leftWheelUp");
	mAnimationStates.push_back(state);
	state = mAvatarScene->GetAnimationState("leftWheelCoverUp");
	mAnimationStates.push_back(state);
	state = mAvatarScene->GetAnimationState("frontWheelUp");
	mAnimationStates.push_back(state);

	//gear down animations
	state = mAvatarScene->GetAnimationState("rightWheelDown");
	mAnimationStates.push_back(state);
	state = mAvatarScene->GetAnimationState("rightWheelCoverDown");
	mAnimationStates.push_back(state);
	state = mAvatarScene->GetAnimationState("leftWheelDown");
	mAnimationStates.push_back(state);
	state = mAvatarScene->GetAnimationState("leftWheelCoverDown");
	mAnimationStates.push_back(state);
	state = mAvatarScene->GetAnimationState("frontWheelDown");
	mAnimationStates.push_back(state);

	closeDoors();
}

void DFACore::DFAAvatar::openDoors()
{
	int count = 0;
	std::list<Ogre::AnimationState*>::iterator iter = mAnimationStates.begin();
	for(;iter != mAnimationStates.end(); iter++)
	{
		if((*iter)->getAnimationName() == "doorLeftLowClose")
		{
			(*iter)->setEnabled(false);
			count++;
		}
		else if((*iter)->getAnimationName() == "doorLeftHighClose")
		{
			(*iter)->setEnabled(false);
			count++;
		}
		else if((*iter)->getAnimationName() == "doorRightLowClose")
		{
			(*iter)->setEnabled(false);
			count++;
		}
		else if((*iter)->getAnimationName() == "doorRightHighClose")
		{
			(*iter)->setEnabled(false);
			count++;
		}

		if(count >= 4)
			break;
	}

	count = 0;
	iter = mAnimationStates.begin();
	for(;iter != mAnimationStates.end(); iter++)
	{
		if((*iter)->getAnimationName() == "doorLeftLowOpen")
		{
			(*iter)->setEnabled(true);
			count++;
		}
		else if((*iter)->getAnimationName() == "doorLeftHighOpen")
		{
			(*iter)->setEnabled(true);
			count++;
		}
		else if((*iter)->getAnimationName() == "doorRightLowOpen")
		{
			(*iter)->setEnabled(true);
			count++;
		}
		else if((*iter)->getAnimationName() == "doorRightHighOpen")
		{
			(*iter)->setEnabled(true);
			count++;
		}

		if(count >= 4)
			break;
	}

	mDoorsOpen = true;
}

void DFACore::DFAAvatar::closeDoors()
{
	int count = 0;
	std::list<Ogre::AnimationState*>::iterator iter = mAnimationStates.begin();
	for(;iter != mAnimationStates.end(); iter++)
	{
		if((*iter)->getAnimationName() == "doorLeftLowOpen")
		{
			(*iter)->setEnabled(false);
			count++;
		}
		else if((*iter)->getAnimationName() == "doorLeftHighOpen")
		{
			(*iter)->setEnabled(false);
			count++;
		}
		else if((*iter)->getAnimationName() == "doorRightLowOpen")
		{
			(*iter)->setEnabled(false);
			count++;
		}
		else if((*iter)->getAnimationName() == "doorRightHighOpen")
		{
			(*iter)->setEnabled(false);
			count++;
		}

		if(count >= 4)
			break;
	}

	count = 0;
	iter = mAnimationStates.begin();
	for(;iter != mAnimationStates.end(); iter++)
	{
		if((*iter)->getAnimationName() == "doorLeftLowClose")
		{
			(*iter)->setEnabled(true);
			count++;
		}
		else if((*iter)->getAnimationName() == "doorLeftHighClose")
		{
			(*iter)->setEnabled(true);
			count++;
		}
		else if((*iter)->getAnimationName() == "doorRightLowClose")
		{
			(*iter)->setEnabled(true);
			count++;
		}
		else if((*iter)->getAnimationName() == "doorRightHighClose")
		{
			(*iter)->setEnabled(true);
			count++;
		}

		if(count >= 4)
			break;
	}

	mDoorsOpen = false;
}

bool DFACore::DFAAvatar::isDoorOpen() const
{
	return mDoorsOpen;
}

void DFACore::DFAAvatar::doorsOpen(bool open)
{
	if(open && !mDoorsOpen)
		openDoors();
	else if(!open && mDoorsOpen)
		closeDoors();
}

void DFACore::DFAAvatar::gearUp()
{
	int count = 0;
	std::list<Ogre::AnimationState*>::iterator iter = mAnimationStates.begin();
	for(;iter != mAnimationStates.end(); iter++)
	{
		if((*iter)->getAnimationName() == "rightWheelDown")
		{
			(*iter)->setEnabled(false);
			count++;
		}
		else if((*iter)->getAnimationName() == "rightWheelCoverDown")
		{
			(*iter)->setEnabled(false);
			count++;
		}
		else if((*iter)->getAnimationName() == "leftWheelDown")
		{
			(*iter)->setEnabled(false);
			count++;
		}
		else if((*iter)->getAnimationName() == "leftWheelCoverDown")
		{
			(*iter)->setEnabled(false);
			count++;
		}
		else if((*iter)->getAnimationName() == "frontWheelDown")
		{
			(*iter)->setEnabled(false);
			count++;
		}

		if(count >= 5)
			break;
	}

	count = 0;
	iter = mAnimationStates.begin();
	for(;iter != mAnimationStates.end(); iter++)
	{
		if((*iter)->getAnimationName() == "rightWheelUp")
		{
			(*iter)->setEnabled(true);
			count++;
		}
		else if((*iter)->getAnimationName() == "rightWheelCoverUp")
		{
			(*iter)->setEnabled(true);
			count++;
		}
		else if((*iter)->getAnimationName() == "leftWheelUp")
		{
			(*iter)->setEnabled(true);
			count++;
		}
		else if((*iter)->getAnimationName() == "leftWheelCoverUp")
		{
			(*iter)->setEnabled(true);
			count++;
		}
		else if((*iter)->getAnimationName() == "frontWheelUp")
		{
			(*iter)->setEnabled(true);
			count++;
		}

		if(count >= 5)
			break;
	}

	mGearUp = true;
}

void DFACore::DFAAvatar::gearDown()
{
	int count = 0;
	std::list<Ogre::AnimationState*>::iterator iter = mAnimationStates.begin();
	for(;iter != mAnimationStates.end(); iter++)
	{
		if((*iter)->getAnimationName() == "rightWheelUp")
		{
			(*iter)->setEnabled(false);
			count++;
		}
		else if((*iter)->getAnimationName() == "rightWheelCoverUp")
		{
			(*iter)->setEnabled(false);
			count++;
		}
		else if((*iter)->getAnimationName() == "leftWheelUp")
		{
			(*iter)->setEnabled(false);
			count++;
		}
		else if((*iter)->getAnimationName() == "leftWheelCoverUp")
		{
			(*iter)->setEnabled(false);
			count++;
		}
		else if((*iter)->getAnimationName() == "frontWheelUp")
		{
			(*iter)->setEnabled(false);
			count++;
		}

		if(count >= 5)
			break;
	}

	count = 0;
	iter = mAnimationStates.begin();
	for(;iter != mAnimationStates.end(); iter++)
	{
		if((*iter)->getAnimationName() == "rightWheelDown")
		{
			(*iter)->setEnabled(true);
			count++;
		}
		else if((*iter)->getAnimationName() == "rightWheelCoverDown")
		{
			(*iter)->setEnabled(true);
			count++;
		}
		else if((*iter)->getAnimationName() == "leftWheelDown")
		{
			(*iter)->setEnabled(true);
			count++;
		}
		else if((*iter)->getAnimationName() == "leftWheelCoverDown")
		{
			(*iter)->setEnabled(true);
			count++;
		}
		else if((*iter)->getAnimationName() == "frontWheelDown")
		{
			(*iter)->setEnabled(true);
			count++;
		}

		if(count >= 5)
			break;
	}

	mGearUp = false;
}

bool DFACore::DFAAvatar::isGearUp() const
{
	return mGearUp;
}

void DFACore::DFAAvatar::gearUp(bool up)
{
	if(up && !mGearUp)
		gearUp();
	else if(!up && mGearUp)
		gearDown();
}

void DFACore::DFAAvatar::loadSounds()
{
	OgreOggSound::OgreOggISound *engineSound = OgreOggSound::OgreOggSoundManager::getSingletonPtr()->createSound("hindEngine", "Hind_Engine.ogg", true, true, false);
	mAvatarNode->attachObject(engineSound);
	engineSound->setMaxDistance(2000);
	//engineSound->setReferenceDistance(250);
	engineSound->play();
}

void DFACore::DFAAvatar::updateAnimations(const Ogre::FrameEvent &evt)
{
	if(mAnimationStates.size() > 0)
	{
		std::list<AnimationState*>::iterator iter = mAnimationStates.begin();
		for(;iter != mAnimationStates.end(); iter++)
		{
			(*iter)->addTime(evt.timeSinceLastFrame);
		}
	}
}

void DFACore::DFAAvatar::createObject(const Ogre::String name, const int id, const Ogre::String &sceneName, Ogre::RenderWindow *renderWindow,
			OgreMax::OgreMaxScene::LoadOptions loadOptions, Ogre::SceneManager *sceneManager)
{
	mName = name;
	mAvatarScene = new OgreMax::OgreMaxScene();
	mAvatarScene->Load(id, sceneName, renderWindow, loadOptions, sceneManager, sceneManager->getRootSceneNode()->createChildSceneNode("AvatarMovementNode")->createChildSceneNode("AvatarNode"));

	mMovementNode = sceneManager->getSceneNode("AvatarMovementNode");
	mAvatarNode = sceneManager->getSceneNode("AvatarNode");
	mCameraNode = mMovementNode->createChildSceneNode("AvatarCameraNode");

	//mMovementNode->setPosition(Vector3(500, 150, 500));

	//set camera node position
	mCameraNode->setPosition(Vector3(0, CAMERA_HEIGHT, -CAMERA_DIST));
	mCameraNode->lookAt(Vector3(0, 0, 500), Node::TS_PARENT);
	mCameraNode->roll(Degree(180));

	//for better testing
	//mAvatarNode->yaw(Degree(-45));
	mAvatarNode->scale(Vector3(1.0,1.0,1.0));

	//Entity *ent = sceneManager->createEntity("Knot", "knot.mesh");
	SceneNode *moveNode = mAvatarNode->createChildSceneNode("AvatarMoveNode");
	moveNode->setPosition(Vector3(0, 0, 100));
	//moveNode->attachObject(ent);
	//moveNode->setScale(0.01, 0.01, 0.01);

	loadAnimations(id, sceneManager);
	loadSounds();
}

Vector3 DFACore::DFAAvatar::getPosition(void)
{
	return mMovementNode->getPosition();
}

double DFACore::DFAAvatar::getMaxTargetRange(void)
{
	return MAX_TARGET_RANGE;
}

void DFACore::DFAAvatar::setTargetList(std::multimap<DFACore::DFAWorldObject*, double> &targetList)
{
	std::multimap<DFACore::DFAWorldObject*, double>::iterator iter = targetList.begin();
	for(;iter != targetList.end(); iter++)
	{
		mTargetList.insert(std::make_pair(iter->first, iter->second));
	}
}

void DFACore::DFAAvatar::shoot(void)
{

}

void DFACore::DFAAvatar::nextWeapon(void)
{
	if(++mCurrentWeapon >= WEAPON_MAX)
		mCurrentWeapon = 0;
}

void DFACore::DFAAvatar::previousWeapon(void)
{
	if(--mCurrentWeapon < 0)
		mCurrentWeapon = WEAPON_MAX;
}

void DFACore::DFAAvatar::selectWeapon(const int num)
{
	if(num >= 0 && num < WEAPON_MAX)
		mCurrentWeapon = num;
}

int DFACore::DFAAvatar::getCurrentWeapon(void)
{
	return mCurrentWeapon;
}

Ogre::String DFACore::DFAAvatar::getNodeName(void)
{
	return mAvatarNode->getName();
}

Ogre::SceneNode* DFACore::DFAAvatar::getCameraNode(void)
{
	return mCameraNode;
}

void DFACore::DFAAvatar::setStartingPosition(const Ogre::Vector3 position)
{
	mMovementNode->setPosition(position);
}

void DFACore::DFAAvatar::lookAt(const Ogre::Vector3 position)
{
	mMovementNode->lookAt(position, Node::TransformSpace::TS_WORLD);
}

void DFACore::DFAAvatar::updatePosition(const FrameEvent &evt)
{
	//pitch
	if(mAccelerate)
	{
		if(mAnglePitch > -PITCH_ANGLE)
			mAnglePitch -= mPitchSpeed;
	}
	else if(mBrake)
	{
		if(mAnglePitch < PITCH_ANGLE)
			mAnglePitch += mPitchSpeed;
	}
	else
	{
		if(mAnglePitch < 0)
			mAnglePitch += mPitchSpeed/2;
		else if(mAnglePitch > 0)
			mAnglePitch -= mPitchSpeed/2;
	}

	//roll
	if(mRollRight)
	{
		if(mAngleRoll < ROLL_ANGLE)
			mAngleRoll += mRollSpeed;
	}
	else if(mRollLeft)
	{
		if(mAngleRoll > -ROLL_ANGLE)
			mAngleRoll -= mRollSpeed;
	}
	else
	{
		if(mAngleRoll > 0.1)
			mAngleRoll -= mRollSpeed/2;
		else if(mAngleRoll < -0.1)
			mAngleRoll += mRollSpeed/2;
		else
			mAngleRoll = 0;
	}

	//yaw
	if(mYawRight)
		mAngleYaw -= mYawSpeed;
	else if(mYawLeft)
		mAngleYaw += mYawSpeed;

	//climb
	mMovementNode->translate(mClimb * evt.timeSinceLastFrame, Node::TS_WORLD);

	//ajust the speed
	Ogre::Real acceleration = -mAnglePitch/90 * 9.8;
	if(Math::Abs(mSpeed) <= MAX_SPEED)
	{
		mSpeed += acceleration;

		if(mSpeed > 0 && mSpeed >= MAX_SPEED)
			mSpeed = MAX_SPEED;
		else if(mSpeed < 0 && Math::Abs(mSpeed) >= MAX_SPEED)
			mSpeed = -MAX_SPEED;
	}

	//adjust rotation
	if(Math::Abs(mSpeed) > TURN_SPEED_THRESHOLD)
	{
		if(mSpeed > 0)
			mAngleYaw -= TURN_SPEED * mAngleRoll/ROLL_ANGLE;
		else if(mSpeed < 0)
			mAngleYaw += TURN_SPEED * mAngleRoll/ROLL_ANGLE;
	}
	else
	{
		if(mSpeed > 0)
			mAngleYaw -= TURN_SPEED * mSpeed/TURN_SPEED_THRESHOLD * mAngleRoll/ROLL_ANGLE;
		else if(mSpeed < 0)
			mAngleYaw -= TURN_SPEED * mSpeed/TURN_SPEED_THRESHOLD * mAngleRoll/ROLL_ANGLE;
	}

	//get the direction
	Ogre::Quaternion orientation = mAvatarNode->getOrientation();
	orientation.normalise();
	Vector3 forward = orientation * Ogre::Vector3(0, 0, 1.0);
	forward.normalise();
	Vector3 right = orientation * Ogre::Vector3(-1.0, 0, 0);
	right.normalise();

	Ogre::Quaternion rollPitch = Quaternion(Degree(mAngleRoll), forward) *
		Quaternion(Degree(mAnglePitch), right);
	Ogre::Quaternion yaw = Quaternion(Degree(mAngleYaw), Vector3::UNIT_Y);
	orientation = rollPitch * yaw;

	Vector3 position = ((SceneNode*)mAvatarNode->getChild("AvatarMoveNode"))->getPosition();
	mDirection = orientation * position;
	mDirection.y = 0;
	mDirection.normalise();

	mAvatarNode->setOrientation(rollPitch);
	mMovementNode->setOrientation(yaw);

	//move to direction
	mMovementNode->translate(mDirection * mSpeed * evt.timeSinceLastFrame, Node::TS_WORLD);
}

void DFACore::DFAAvatar::accelerate(bool pressed)
{
	if(pressed)
	{
		mAccelerate = true;
		mBrake = false;
	}
	else
	{
		mAccelerate = false;
		mBrake = false;
	}
}

void DFACore::DFAAvatar::brake(bool pressed)
{
	if(pressed)
	{
		mBrake = true;
		mAccelerate = false;
	}
	else
	{
		mBrake = false;
		mAccelerate = false;
	}
}

void DFACore::DFAAvatar::moveLeft(bool pressed)
{
	if(pressed)
	{
		mRollLeft = true;
		mRollRight = false;
	}
	else
	{
		mRollRight = false;
		mRollLeft = false;
	}
}

void DFACore::DFAAvatar::moveRight(bool pressed)
{
	if(pressed)
	{
		mRollRight = true;
		mRollLeft = false;
	}
	else
	{
		mRollRight = false;
		mRollLeft = false;
	}
}

void DFACore::DFAAvatar::rotateLeft(bool pressed)
{
	if(pressed)
	{
		mYawLeft = true;
		mYawRight = false;
	}
	else
	{
		mYawRight = false;
		mYawLeft = false;
	}
}

void DFACore::DFAAvatar::rotateRight(bool pressed)
{
	if(pressed)
	{
		mYawRight = true;
		mYawLeft = false;
	}
	else
	{
		mYawRight = false;
		mYawLeft = false;
	}
}

void DFACore::DFAAvatar::moveUp(bool pressed)
{
	if(pressed)
		mClimb.y = MAX_CLIMB_SPEED;
	else
		mClimb.y = 0;
}

void DFACore::DFAAvatar::moveDown(bool pressed)
{
	if(pressed)
		mClimb.y = -MAX_CLIMB_SPEED;
	else
		mClimb.y = 0;
}

Ogre::String DFACore::DFAAvatar::getName(void)
{
	return mName;
}

DFACore::DFAAvatar* DFACore::DFAAvatar::getSingletonPtr(void)
{
	if(!mAvatar)
	{
		mAvatar = new DFACore::DFAAvatar();
	}

	return mAvatar;
	
	//return ms_Singleton;
}