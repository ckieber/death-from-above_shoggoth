#include "DFAWorldData.h"

#include "DFAAvatar.h"

using namespace Ogre;

DFAData::DFAWorldData *DFAData::DFAWorldData::mWorldData = 0;

DFAData::DFAWorldData::DFAWorldData()
{
	
}

DFAData::DFAWorldData::~DFAWorldData()
{
	
}

std::multimap<DFACore::DFAWorldObject*, double> DFAData::DFAWorldData::updateTargetList(const FrameEvent &evt, std::list<DFACore::DFAWorldObject*> &worldObjects, Ogre::SceneManager *sceneMgr)
{
	if(worldObjects.size() > 0)
	{
		mDistanceData.clear();
		std::list<DFACore::DFAWorldObject*>::iterator iter = worldObjects.begin();
		const Vector3 avatarPosition = (*iter)->getPosition();
		const double maxTargetRange = pow(((DFACore::DFAAvatar*)(*iter))->getMaxTargetRange(), 2);
		iter++;
		
		for(; iter != worldObjects.end(); iter++)
		{
			const Vector3 objectPosition = (*iter)->getPosition();
			const double distance = avatarPosition.squaredDistance(objectPosition);
			if(maxTargetRange >= distance)
				mDistanceData.insert(std::make_pair(*iter, distance));
		}
	}

	return mDistanceData;
}

DFAData::DFAWorldData* DFAData::DFAWorldData::getSingletonPtr(void)
{
	if(!mWorldData)
	{
		mWorldData = new DFAData::DFAWorldData();
	}

	return mWorldData;
	
	//return ms_Singleton;
}