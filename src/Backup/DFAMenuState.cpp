#include <Ogre.h>

#include "DFAMenuState.h"
#include "DFAPauseState.h"
#include "DFAPlayState.h"
#include "DFAPilotManager.h"

using namespace Ogre;

DFACore::DFAMenuState DFACore::DFAMenuState::mMenuState;

void DFACore::DFAMenuState::enter()
{
	mRoot = Root::getSingletonPtr();
	mKeyboard = DFAInput::DFAInputManager::getSingletonPtr()->getKeyboard();
	mMouse = DFAInput::DFAInputManager::getSingletonPtr()->getMouse();

	mSceneMgr = mRoot->createSceneManager(ST_GENERIC);
	mCamera = mSceneMgr->createCamera("MenuCamera");
	mViewport = mRoot->getAutoCreatedWindow()->addViewport(mCamera);

	mSoundManager = mSoundManager = OgreOggSound::OgreOggSoundManager::getSingletonPtr();
	mSoundManager->init();
	mTheme = mSoundManager->createSound("menuTheme", "Theme.ogg", true, true, false);
	mTheme->play();

	DFAData::DFAPilotManager::getSingletonPtr()->loadDataFromFile();
	initGUI();
	loadMainMenu();

	/*manually create the quit button
	CEGUI::WindowManager *win = CEGUI::WindowManager::getSingletonPtr();
	CEGUI::Window *sheet = win->createWindow("DefaultGUISheet", "CEGUIDemo/Sheet");

	CEGUI::Window *quit = win->createWindow("TaharezLook/Button", "CEGUIDemo/QuitButton");
	quit->setText("Quit");
	quit->setSize(CEGUI::UVector2(CEGUI::UDim(0.15, 0), CEGUI::UDim(0.05, 0)));

	quit->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&DFACore::DFAMenuState::quit, this));

	sheet->addChildWindow(quit);
	mSystem->setGUISheet(sheet);
	*/

	mExitGame = false;
}

void DFACore::DFAMenuState::exit()
{
	mTheme->stop();

	mSceneMgr->clearScene();
	mSceneMgr->destroyAllCameras();
	mRoot->getAutoCreatedWindow()->removeAllViewports();
}

void DFACore::DFAMenuState::pause()
{
}

void DFACore::DFAMenuState::resume()
{
}

bool DFACore::DFAMenuState::keyClicked(const OIS::KeyEvent &e)
{
	return true;
}

bool DFACore::DFAMenuState::keyPressed(const OIS::KeyEvent &e)
{
	CEGUI::System *sys = CEGUI::System::getSingletonPtr();
	sys->injectKeyDown(e.key);
	sys->injectChar(e.text);
	return true;
}

bool DFACore::DFAMenuState::keyReleased(const OIS::KeyEvent &e)
{
	CEGUI::System::getSingleton().injectKeyUp(e.key);
	return true;
}

bool DFACore::DFAMenuState::mouseMoved(const OIS::MouseEvent &e)
{
	CEGUI::System::getSingleton().injectMouseMove(e.state.X.rel, e.state.Y.rel);
	return true;
}

bool DFACore::DFAMenuState::mousePressed(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
	CEGUI::System::getSingleton().injectMouseButtonDown(convertButton(id));
	return true;
}

bool DFACore::DFAMenuState::mouseReleased(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
	CEGUI::System::getSingleton().injectMouseButtonUp(convertButton(id));
	return true;
}

bool DFACore::DFAMenuState::frameStarted(const FrameEvent &evt)
{
	DFAInput::DFAInputManager::getSingletonPtr()->capture();
	return !mExitGame && !mKeyboard->isKeyDown(OIS::KC_ESCAPE);
}

bool DFACore::DFAMenuState::frameEnded(const FrameEvent &evt)
{
	return !mExitGame;
}

CEGUI::MouseButton DFACore::DFAMenuState::convertButton(OIS::MouseButtonID buttonID)
{
	switch(buttonID)
	{
		case OIS::MB_Left:
			return CEGUI::LeftButton;
		case OIS::MB_Middle:
			return CEGUI::MiddleButton;
		case OIS::MB_Right:
			return CEGUI::RightButton;
		default:
			return CEGUI::LeftButton;
	}
}

void DFACore::DFAMenuState::initGUI()
{
	mRenderer = new CEGUI::OgreCEGUIRenderer(mRoot->getAutoCreatedWindow(), Ogre::RENDER_QUEUE_OVERLAY, false, 3000, mSceneMgr);
	mSystem = new CEGUI::System(mRenderer);

	CEGUI::SchemeManager::getSingleton().loadScheme((CEGUI::utf8*)"TaharezLookSkin.scheme");
	mSystem->setDefaultMouseCursor((CEGUI::utf8*)"TaharezLook", (CEGUI::utf8*)"MouseArrow");
	mSystem->setDefaultFont((CEGUI::utf8*)"BlueHighway-12");
	CEGUI::MouseCursor::getSingleton().setImage(CEGUI::System::getSingleton().getDefaultMouseCursor());

	CEGUI::ImagesetManager::getSingletonPtr()->createImagesetFromImageFile("BackgroundImage", "Background.tga");
	CEGUI::ImagesetManager::getSingletonPtr()->createImagesetFromImageFile("CreditsImage", "Credits.jpg");

	//load all sheets


}

bool DFACore::DFAMenuState::quitMainMenu(const CEGUI::EventArgs &e)
{
	mExitGame = true;
	DFAData::DFAPilotManager::getSingletonPtr()->saveDataToFile();
	return !mExitGame;
}

bool DFACore::DFAMenuState::startMissionMainMenu(const CEGUI::EventArgs &e)
{
	DFAData::DFAPilotManager::getSingletonPtr()->saveDataToFile();
	changeState(DFACore::DFAPlayState::getInstance());
	return true;
}

void DFACore::DFAMenuState::subscribeMainMenuButtons()
{
	CEGUI::Window *startMission = CEGUI::WindowManager::getSingletonPtr()->getWindow("imageBackground/startMission");
	startMission->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&DFACore::DFAMenuState::startMissionMainMenu, this));

	CEGUI::Window *pilotLog = CEGUI::WindowManager::getSingletonPtr()->getWindow("imageBackground/pilotLog");
	pilotLog->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&DFACore::DFAMenuState::loadPilotLogMenu, this));

	CEGUI::Window *settings = CEGUI::WindowManager::getSingletonPtr()->getWindow("imageBackground/settings");
	settings->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&DFACore::DFAMenuState::loadSettingsMenu, this));

	CEGUI::Window *credits = CEGUI::WindowManager::getSingletonPtr()->getWindow("imageBackground/credits");
	credits->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&DFACore::DFAMenuState::loadCreditsMenu, this));

	CEGUI::Window *quit = CEGUI::WindowManager::getSingletonPtr()->getWindow("imageBackground/quit");
	quit->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&DFACore::DFAMenuState::quitMainMenu, this));
}

void DFACore::DFAMenuState::subscribePilotLogButtons()
{
	CEGUI::Window *startMission = CEGUI::WindowManager::getSingletonPtr()->getWindow("imageBackground/startMission");
	startMission->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&DFACore::DFAMenuState::startMissionMainMenu, this));

	CEGUI::Window *mainMenu = CEGUI::WindowManager::getSingletonPtr()->getWindow("imageBackground/mainMenu");
	mainMenu->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&DFACore::DFAMenuState::loadMainMenu, this));

	CEGUI::Window *settings = CEGUI::WindowManager::getSingletonPtr()->getWindow("imageBackground/settings");
	settings->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&DFACore::DFAMenuState::loadSettingsMenu, this));

	CEGUI::Window *credits = CEGUI::WindowManager::getSingletonPtr()->getWindow("imageBackground/credits");
	credits->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&DFACore::DFAMenuState::loadCreditsMenu, this));

	CEGUI::Window *quit = CEGUI::WindowManager::getSingletonPtr()->getWindow("imageBackground/quit");
	quit->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&DFACore::DFAMenuState::quitMainMenu, this));

	CEGUI::Window *pilot = CEGUI::WindowManager::getSingletonPtr()->getWindow("imageBackground/frameWindow/frameWindowPilots/");
	pilot->subscribeEvent(CEGUI::Listbox::EventSelectionChanged, CEGUI::Event::Subscriber(&DFACore::DFAMenuState::handlePilotSelection, this));

	CEGUI::Window *selectPilot = CEGUI::WindowManager::getSingletonPtr()->getWindow("imageBackground/frameWindow/selectPilot");
	selectPilot->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&DFACore::DFAMenuState::selectPilotPilotLog, this));

	CEGUI::Window *addPilot = CEGUI::WindowManager::getSingletonPtr()->getWindow("imageBackground/frameWindow/addPilot");
	addPilot->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&DFACore::DFAMenuState::addPilotPilotLog, this));

	CEGUI::Window *renamePilot = CEGUI::WindowManager::getSingletonPtr()->getWindow("imageBackground/frameWindow/renamePilot");
	renamePilot->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&DFACore::DFAMenuState::renamePilotPilotLog, this));

	CEGUI::Window *resetPilot = CEGUI::WindowManager::getSingletonPtr()->getWindow("imageBackground/frameWindow/resetPilot");
	resetPilot->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&DFACore::DFAMenuState::resetPilotPilotLog, this));

	CEGUI::Window *resetAll = CEGUI::WindowManager::getSingletonPtr()->getWindow("imageBackground/frameWindow/resetAll");
	resetAll->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&DFACore::DFAMenuState::resetAllPilotLog, this));

	CEGUI::Window *deletePilot = CEGUI::WindowManager::getSingletonPtr()->getWindow("imageBackground/frameWindow/deletePilot");
	deletePilot->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&DFACore::DFAMenuState::deletePilotPilotLog, this));

	CEGUI::Window *deleteAll = CEGUI::WindowManager::getSingletonPtr()->getWindow("imageBackground/frameWindow/deleteAll");
	deleteAll->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&DFACore::DFAMenuState::deleteAllPilotLog, this));
}

bool DFACore::DFAMenuState::selectPilotPilotLog(const CEGUI::EventArgs &e)
{
	CEGUI::Listbox *pilotList = (CEGUI::Listbox*)CEGUI::WindowManager::getSingletonPtr()->getWindow("imageBackground/frameWindow/frameWindowPilots/");
	CEGUI::ListboxItem *item = pilotList->getFirstSelectedItem();

	if(item)
	{
		CEGUI::DefaultWindow *activePilot = (CEGUI::DefaultWindow*)CEGUI::WindowManager::getSingleton().getWindow("imageBackground/currPilotWindow/pilot");
		activePilot->setText(item->getText());
		DFAData::DFAPilotManager::getSingletonPtr()->setActivePilot(item->getText());
	}

	return true;
}

bool DFACore::DFAMenuState::addPilotPilotLog(const CEGUI::EventArgs &e)
{
	CEGUI::DefaultWindow *nameBox = (CEGUI::DefaultWindow*)CEGUI::WindowManager::getSingleton().getWindow("imageBackground/frameWindow/nameWindow/nameBox");
	if(nameBox->getText() != "")
	{
		DFAData::DFAPilotManager *pilotManager = DFAData::DFAPilotManager::getSingletonPtr();
		if(!pilotManager->exists(nameBox->getText()))
		{
			pilotManager->createPilot(nameBox->getText());
			
			//add this pilot to the listBox
			CEGUI::Listbox *list = (CEGUI::Listbox*)CEGUI::WindowManager::getSingleton().getWindow("imageBackground/frameWindow/frameWindowPilots/");
			CEGUI::ListboxTextItem* item = new CEGUI::ListboxTextItem(nameBox->getText());
			list->addItem(item);
			DFAData::DFAPilotManager::getSingletonPtr()->saveDataToFile();
		}
	}
	
	return true;
}

bool DFACore::DFAMenuState::renamePilotPilotLog(const CEGUI::EventArgs &e)
{
	CEGUI::DefaultWindow *nameBox = (CEGUI::DefaultWindow*)CEGUI::WindowManager::getSingleton().getWindow("imageBackground/frameWindow/nameWindow/nameBox");
	if(nameBox->getText() != "")
	{
		DFAData::DFAPilotManager *pilotManager = DFAData::DFAPilotManager::getSingletonPtr();
		if(!pilotManager->exists(nameBox->getText()))
		{
			CEGUI::Listbox *pilotList = (CEGUI::Listbox*)CEGUI::WindowManager::getSingletonPtr()->getWindow("imageBackground/frameWindow/frameWindowPilots/");
			CEGUI::ListboxItem *item = pilotList->getFirstSelectedItem();

			if(item->getText() != "Default Pilot")
			{
				pilotManager->getPilot(item->getText())->setName(nameBox->getText());
				// also update the listBox
				item->setText(nameBox->getText());
				pilotList->requestRedraw();
				DFAData::DFAPilotManager::getSingletonPtr()->saveDataToFile();
			}
		}
	}

	return true;
}

bool DFACore::DFAMenuState::resetPilotPilotLog(const CEGUI::EventArgs &e)
{
	CEGUI::Listbox *pilotList = (CEGUI::Listbox*)CEGUI::WindowManager::getSingletonPtr()->getWindow("imageBackground/frameWindow/frameWindowPilots/");
	CEGUI::ListboxItem *item = pilotList->getFirstSelectedItem();
	
	if(item)
	{
		DFAData::DFAPilotManager *pilotManager = DFAData::DFAPilotManager::getSingletonPtr();
		pilotManager->getPilot(item->getText())->resetPilot();

		// refresh statistic window
		CEGUI::DefaultWindow *stats = (CEGUI::DefaultWindow*)CEGUI::WindowManager::getSingletonPtr()->getWindow("imageBackground/frameWindow/statisticWindow/statistic");
		stats->setText(pilotManager->printPilot(item->getText()));
	}

	return true;
}

bool DFACore::DFAMenuState::resetAllPilotLog(const CEGUI::EventArgs &e)
{
	DFAData::DFAPilotManager *pilotManager = DFAData::DFAPilotManager::getSingletonPtr();
	std::list<DFAData::DFAPilot*> *pilotList = pilotManager->getPilotList();
	std::list<DFAData::DFAPilot*>::iterator iter = pilotList->begin();

	for(; iter != pilotList->end(); iter++)
	{
		(*iter)->resetPilot();
	}

	// if there is an item selected refresh it imediately
	CEGUI::Listbox *list = (CEGUI::Listbox*)CEGUI::WindowManager::getSingletonPtr()->getWindow("imageBackground/frameWindow/frameWindowPilots/");
	CEGUI::ListboxItem *item = list->getFirstSelectedItem();

	if(item)
	{
		// refresh statistic window
		CEGUI::DefaultWindow *stats = (CEGUI::DefaultWindow*)CEGUI::WindowManager::getSingletonPtr()->getWindow("imageBackground/frameWindow/statisticWindow/statistic");
		stats->setText(pilotManager->printPilot(item->getText()));
	}

	return true;
}

bool DFACore::DFAMenuState::deletePilotPilotLog(const CEGUI::EventArgs &e)
{
	CEGUI::Listbox *list = (CEGUI::Listbox*)CEGUI::WindowManager::getSingletonPtr()->getWindow("imageBackground/frameWindow/frameWindowPilots/");
	CEGUI::ListboxItem *item = list->getFirstSelectedItem();

	if(item)
	{
		DFAData::DFAPilotManager *pilotManager = DFAData::DFAPilotManager::getSingletonPtr();
		CEGUI::String activePilotName = pilotManager->getActivePilot()->getName();

		if(pilotManager->deletePilot(item->getText()))
		{
			// if this pilot is active pilot -> make default pilot active
			if(activePilotName == item->getText())
			{
				pilotManager->setActivePilot("Default Pilot");
				CEGUI::DefaultWindow *activePilot = (CEGUI::DefaultWindow*)CEGUI::WindowManager::getSingleton().getWindow("imageBackground/currPilotWindow/pilot");
				activePilot->setText(pilotManager->getActivePilot()->getName());
			}

			// update list and stats box
			CEGUI::DefaultWindow *stats = (CEGUI::DefaultWindow*)CEGUI::WindowManager::getSingletonPtr()->getWindow("imageBackground/frameWindow/statisticWindow/statistic");
			CEGUI::DefaultWindow *nameBox = (CEGUI::DefaultWindow*)CEGUI::WindowManager::getSingleton().getWindow("imageBackground/frameWindow/nameWindow/nameBox");

			nameBox->setText("");
			stats->setText("");
			list->removeItem(item);
		}
	}
	
	return true;
}

bool DFACore::DFAMenuState::deleteAllPilotLog(const CEGUI::EventArgs &e)
{
	DFAData::DFAPilotManager *pilotManager = DFAData::DFAPilotManager::getSingletonPtr();
	
	// set default pilot as active pilot
	pilotManager->setActivePilot("Default Pilot");
	CEGUI::DefaultWindow *activePilot = (CEGUI::DefaultWindow*)CEGUI::WindowManager::getSingleton().getWindow("imageBackground/currPilotWindow/pilot");
	activePilot->setText(pilotManager->getActivePilot()->getName());
	
	pilotManager->deleteAllPilots();

	// update list and static window
	CEGUI::Listbox *list = (CEGUI::Listbox*)CEGUI::WindowManager::getSingletonPtr()->getWindow("imageBackground/frameWindow/frameWindowPilots/");
	CEGUI::ListboxItem *item = list->getFirstSelectedItem();

	if(item)
	{
		// clear statistic window only if the selected pilot is not the default pilot
		if(item->getText() != "Default Pilot")
		{
			CEGUI::DefaultWindow *stats = (CEGUI::DefaultWindow*)CEGUI::WindowManager::getSingletonPtr()->getWindow("imageBackground/frameWindow/statisticWindow/statistic");
			CEGUI::DefaultWindow *nameBox = (CEGUI::DefaultWindow*)CEGUI::WindowManager::getSingleton().getWindow("imageBackground/frameWindow/nameWindow/nameBox");

			nameBox->setText("");
			stats->setText("");
		}
	}

	// remove all list items except the default pilot
	while(list->getItemCount() > 1)
	{
		item = list->getListboxItemFromIndex(0);
		if(item->getText() == "Default Pilot")
		{
			item = list->getListboxItemFromIndex(1);
			list->removeItem(item);
		}
		else
			list->removeItem(item);
	}

	return true;
}

bool DFACore::DFAMenuState::handlePilotSelection(const CEGUI::EventArgs &e)
{
	//get selected item
	const CEGUI::WindowEventArgs &windowEventArgs = static_cast<const CEGUI::WindowEventArgs&>(e);
	CEGUI::ListboxItem *item = static_cast<CEGUI::Listbox*>(windowEventArgs.window)->getFirstSelectedItem();

	if(item)
	{
		//add the selection colour
		item->setSelectionColours(CEGUI::colour(192, 192, 192, 1));
		item->setSelectionBrushImage("TaharezLook", "ListboxSelectionBrush");

		//if the item exists add it to statistic box
		DFAData::DFAPilotManager *pilotManager = DFAData::DFAPilotManager::getSingletonPtr();
		CEGUI::DefaultWindow *textBox = (CEGUI::DefaultWindow*)CEGUI::WindowManager::getSingleton().getWindow("imageBackground/frameWindow/statisticWindow/statistic");
		textBox->setText(pilotManager->printPilot(item->getText()));

		CEGUI::DefaultWindow *nameBox = (CEGUI::DefaultWindow*)CEGUI::WindowManager::getSingleton().getWindow("imageBackground/frameWindow/nameWindow/nameBox");
		nameBox->setText(item->getText());
	}
	else
	{
		CEGUI::DefaultWindow *textBox = (CEGUI::DefaultWindow*)CEGUI::WindowManager::getSingleton().getWindow("imageBackground/frameWindow/statisticWindow/statistic");
		textBox->setText("");

		CEGUI::DefaultWindow *nameBox = (CEGUI::DefaultWindow*)CEGUI::WindowManager::getSingleton().getWindow("imageBackground/frameWindow/nameWindow/nameBox");
		nameBox->setText("");
	}

	return true;
}

void DFACore::DFAMenuState::subscribeSettingsButtons()
{
	CEGUI::Window *startMission = CEGUI::WindowManager::getSingletonPtr()->getWindow("imageBackground/startMission");
	startMission->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&DFACore::DFAMenuState::startMissionMainMenu, this));

	CEGUI::Window *pilotLog = CEGUI::WindowManager::getSingletonPtr()->getWindow("imageBackground/pilotLog");
	pilotLog->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&DFACore::DFAMenuState::loadPilotLogMenu, this));

	CEGUI::Window *mainMenu = CEGUI::WindowManager::getSingletonPtr()->getWindow("imageBackground/mainMenu");
	mainMenu->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&DFACore::DFAMenuState::loadMainMenu, this));

	CEGUI::Window *credits = CEGUI::WindowManager::getSingletonPtr()->getWindow("imageBackground/credits");
	credits->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&DFACore::DFAMenuState::loadCreditsMenu, this));

	CEGUI::Window *quit = CEGUI::WindowManager::getSingletonPtr()->getWindow("imageBackground/quit");
	quit->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&DFACore::DFAMenuState::quitMainMenu, this));

	CEGUI::Window *keyAssign = CEGUI::WindowManager::getSingletonPtr()->getWindow("imageBackground/frameWindow/keyWindow/keyList");
	keyAssign->subscribeEvent(CEGUI::Listbox::EventSelectionChanged, CEGUI::Event::Subscriber(&DFACore::DFAMenuState::handleKeySelection, this));
}

bool DFACore::DFAMenuState::handleKeySelection(const CEGUI::EventArgs &e)
{
	//get selected item
	const CEGUI::WindowEventArgs &windowEventArgs = static_cast<const CEGUI::WindowEventArgs&>(e);
	CEGUI::ListboxItem *item = static_cast<CEGUI::Listbox*>(windowEventArgs.window)->getFirstSelectedItem();

	if(item)
	{
		//add the selection colour
		item->setSelectionColours(CEGUI::colour(192, 192, 192, 1));
		item->setSelectionBrushImage("TaharezLook", "ListboxSelectionBrush");

		//if the item exists add it to statistic box
		//DFAData::DFAPilotManager *pilotManager = DFAData::DFAPilotManager::getSingletonPtr();
		//CEGUI::DefaultWindow *textBox = (CEGUI::DefaultWindow*)CEGUI::WindowManager::getSingleton().getWindow("imageBackground/frameWindow/statisticWindow/statistic");
		//textBox->setText(pilotManager->printPilot(item->getText()));
	}

	return true;
}

void DFACore::DFAMenuState::subscribeCreditsButtons()
{
	CEGUI::Window *startMission = CEGUI::WindowManager::getSingletonPtr()->getWindow("imageBackground/startMission");
	startMission->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&DFACore::DFAMenuState::startMissionMainMenu, this));

	CEGUI::Window *pilotLog = CEGUI::WindowManager::getSingletonPtr()->getWindow("imageBackground/pilotLog");
	pilotLog->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&DFACore::DFAMenuState::loadPilotLogMenu, this));

	CEGUI::Window *settings = CEGUI::WindowManager::getSingletonPtr()->getWindow("imageBackground/settings");
	settings->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&DFACore::DFAMenuState::loadSettingsMenu, this));

	CEGUI::Window *mainMenu = CEGUI::WindowManager::getSingletonPtr()->getWindow("imageBackground/mainMenu");
	mainMenu->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&DFACore::DFAMenuState::loadMainMenu, this));

	CEGUI::Window *quit = CEGUI::WindowManager::getSingletonPtr()->getWindow("imageBackground/quit");
	quit->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&DFACore::DFAMenuState::quitMainMenu, this));
}

void DFACore::DFAMenuState::loadMainMenu()
{
	CEGUI::WindowManager::getSingleton().destroyAllWindows();
	CEGUI::Window *sheet = CEGUI::WindowManager::getSingleton().loadWindowLayout((CEGUI::utf8*)"mainMenu.layout");
	
	//load image into the staticImage holder
	CEGUI::Window *background = CEGUI::WindowManager::getSingleton().getWindow("imageBackground");
	CEGUI::DefaultWindow *staticImage = static_cast<CEGUI::DefaultWindow*>(background);
	staticImage->setProperty("Image", "set:BackgroundImage image:full_image");

	subscribeMainMenuButtons();

	mSystem->setGUISheet(sheet);

	CEGUI::DefaultWindow *activePilot = (CEGUI::DefaultWindow*)CEGUI::WindowManager::getSingleton().getWindow("imageBackground/currPilotWindow/pilot");
	DFAData::DFAPilotManager *pilotManager = DFAData::DFAPilotManager::getSingletonPtr();

	if(pilotManager->getActivePilot())
		activePilot->setText(pilotManager->getActivePilot()->getName());
	else
	{
		pilotManager->setActivePilot("Default Pilot");
		activePilot->setText(pilotManager->getActivePilot()->getName());
	}

}

bool DFACore::DFAMenuState::loadMainMenu(const CEGUI::EventArgs &e)
{
	loadMainMenu();
	return true;
}

bool DFACore::DFAMenuState::loadPilotLogMenu(const CEGUI::EventArgs &e)
{
	CEGUI::WindowManager::getSingleton().destroyAllWindows();
	CEGUI::Window *sheet = CEGUI::WindowManager::getSingleton().loadWindowLayout((CEGUI::utf8*)"pilotLog.layout");
	
	//load image into the staticImage holder
	CEGUI::Window *background = CEGUI::WindowManager::getSingleton().getWindow("imageBackground");
	CEGUI::DefaultWindow *staticImage = static_cast<CEGUI::DefaultWindow*>(background);
	staticImage->setProperty("Image", "set:BackgroundImage image:full_image");

	CEGUI::Listbox *list = (CEGUI::Listbox*)CEGUI::WindowManager::getSingleton().getWindow("imageBackground/frameWindow/frameWindowPilots/");

	DFAData::DFAPilotManager *pilotManager = DFAData::DFAPilotManager::getSingletonPtr();
	//pilotManager->loadDataFromFile();

	/*
	DFAData::DFAPilot *newPilot = pilotManager->createPilot("Christian Kieber");
	newPilot->addKills(11);
	newPilot->increaseCompMissionsNight();
	newPilot->increaseCompMissionsNight();
	newPilot->increaseCompMissionsDay();
	newPilot->increaseIncompMissionsNight();
	newPilot->setStatus(DFAData::DFAPilot::INJURED);

	newPilot = pilotManager->createPilot("Jeremy Weber");
	newPilot->promote();
	*/
	//testing
	//pilotManager->saveDataToFile();

	// add all the pilots in mPilots to the list
	std::list<DFAData::DFAPilot*> *pilotList = pilotManager->getPilotList();
	std::list<DFAData::DFAPilot*>::iterator iter = pilotList->begin();
	for(; iter != pilotList->end(); iter++)
	{
		CEGUI::ListboxTextItem* item = new CEGUI::ListboxTextItem((*iter)->getName());
		list->addItem(item);
	}

	subscribePilotLogButtons();

	mSystem->setGUISheet(sheet);

	CEGUI::DefaultWindow *activePilot = (CEGUI::DefaultWindow*)CEGUI::WindowManager::getSingleton().getWindow("imageBackground/currPilotWindow/pilot");
	activePilot->setText(DFAData::DFAPilotManager::getSingletonPtr()->getActivePilot()->getName());

	return true;
}

bool DFACore::DFAMenuState::loadSettingsMenu(const CEGUI::EventArgs &e)
{
	CEGUI::WindowManager::getSingleton().destroyAllWindows();
	CEGUI::Window *sheet = CEGUI::WindowManager::getSingleton().loadWindowLayout((CEGUI::utf8*)"settings.layout");
	
	//load image into the staticImage holder
	CEGUI::Window *background = CEGUI::WindowManager::getSingleton().getWindow("imageBackground");
	CEGUI::DefaultWindow *staticImage = static_cast<CEGUI::DefaultWindow*>(background);
	staticImage->setProperty("Image", "set:BackgroundImage image:full_image");

	//just for testing
	CEGUI::Listbox *list = (CEGUI::Listbox*)CEGUI::WindowManager::getSingleton().getWindow("imageBackground/frameWindow/keyWindow/keyList");

	CEGUI::ListboxTextItem* item = new CEGUI::ListboxTextItem("Forwards ---> Up Arrow");
	list->addItem(item);
	item = new CEGUI::ListboxTextItem("Backwards ---> Down Arrow");
	list->addItem(item);
	item = new CEGUI::ListboxTextItem("Roll left ---> Left Arrow");
	list->addItem(item);
	item = new CEGUI::ListboxTextItem("Roll right ---> Right Arrow");
	list->addItem(item);
	item = new CEGUI::ListboxTextItem("Up ---> Page Up");
	list->addItem(item);
	item = new CEGUI::ListboxTextItem("Down ---> Page Down");
	list->addItem(item);
	//stop testing

	subscribeSettingsButtons();

	mSystem->setGUISheet(sheet);

	CEGUI::DefaultWindow *activePilot = (CEGUI::DefaultWindow*)CEGUI::WindowManager::getSingleton().getWindow("imageBackground/currentPilotWindow/pilot");
	activePilot->setText(DFAData::DFAPilotManager::getSingletonPtr()->getActivePilot()->getName());

	return true;
}

bool DFACore::DFAMenuState::loadCreditsMenu(const CEGUI::EventArgs &e)
{
	CEGUI::WindowManager::getSingleton().destroyAllWindows();
	CEGUI::Window *sheet = CEGUI::WindowManager::getSingleton().loadWindowLayout((CEGUI::utf8*)"credits.layout");
	
	//load image into the staticImage holder
	CEGUI::Window *background = CEGUI::WindowManager::getSingleton().getWindow("imageBackground");
	CEGUI::DefaultWindow *staticImage = static_cast<CEGUI::DefaultWindow*>(background);
	staticImage->setProperty("Image", "set:BackgroundImage image:full_image");

	CEGUI::Window *creditsImage = CEGUI::WindowManager::getSingleton().getWindow("imageBackground/frameWindow/picture");
	staticImage = static_cast<CEGUI::DefaultWindow*>(creditsImage);
	staticImage->setProperty("Image", "set:CreditsImage image:full_image");

	subscribeCreditsButtons();

	mSystem->setGUISheet(sheet);

	CEGUI::DefaultWindow *activePilot = (CEGUI::DefaultWindow*)CEGUI::WindowManager::getSingleton().getWindow("imageBackground/currPilotWindow/pilot");
	activePilot->setText(DFAData::DFAPilotManager::getSingletonPtr()->getActivePilot()->getName());

	return true;
}