#ifndef __DFAMenuState__
#define __DFAMenuState__

#include <Ogre.h>
#include <CEGUI/CEGUI.h>
#include <OgreCEGUIRenderer.h>

#include "DFAGameState.h"

namespace DFACore
{
	class DFAMenuState : public DFAGameState
	{
	public:
		void enter();
		void exit();

		void pause();
		void resume();

		bool keyClicked(const OIS::KeyEvent &e);
		bool keyPressed(const OIS::KeyEvent &e);
		bool keyReleased(const OIS::KeyEvent &e);

		bool mouseMoved(const OIS::MouseEvent &e);
		bool mousePressed(const OIS::MouseEvent &e, OIS::MouseButtonID id);
		bool mouseReleased(const OIS::MouseEvent &e, OIS::MouseButtonID id);

		bool frameStarted(const Ogre::FrameEvent &evt);
		bool frameEnded(const Ogre::FrameEvent &evt);

		CEGUI::MouseButton convertButton(OIS::MouseButtonID buttonID);

		void initGUI();

		void loadMainMenu();
		bool loadMainMenu(const CEGUI::EventArgs &e);
		void subscribeMainMenuButtons();
		// main menu buttons
		bool startMissionMainMenu(const CEGUI::EventArgs &e);
		bool quitMainMenu(const CEGUI::EventArgs &e);

		bool loadPilotLogMenu(const CEGUI::EventArgs &e);
		void subscribePilotLogButtons();
		bool handlePilotSelection(const CEGUI::EventArgs &e);
		// pilot log buttons

		bool loadSettingsMenu(const CEGUI::EventArgs &e);
		void subscribeSettingsButtons();
		bool handleKeySelection(const CEGUI::EventArgs &e);

		bool loadCreditsMenu(const CEGUI::EventArgs &e);
		void subscribeCreditsButtons();

		static DFAMenuState* getInstance()
		{
			return &mMenuState;
		}
	protected:
		DFAMenuState() { }

		Ogre::Root *mRoot;
		Ogre::SceneManager *mSceneMgr;
		Ogre::Viewport *mViewport;
		Ogre::Camera *mCamera;
		OIS::Keyboard *mKeyboard;
		OIS::Mouse *mMouse;
		CEGUI::System *mSystem;
		CEGUI::OgreCEGUIRenderer *mRenderer;
		bool mExitGame;
	private:
		static DFAMenuState mMenuState;
	};
};

#endif