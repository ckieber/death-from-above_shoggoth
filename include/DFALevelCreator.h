#ifndef __DFALevelCreator__
#define __DFALevelCreator__

#include <Ogre.h>

#include "DFAWorldObject.h"
#include "OgreOggSound.h"

namespace DFAData
{
	class DFALevelCreator
	{
	public:
		void createLevel(const int levelNumber, Ogre::SceneManager *sceneMgr, Ogre::RenderWindow *renderWindow,
												OgreOggSound::OgreOggSoundManager *soundManager, Ogre::Camera *camera);
		Ogre::Vector3 getLevelOffset(const int levelNumber);
		Ogre::String getTerrainNodeName(const int levelNumber);
		static DFALevelCreator* getSingletonPtr(void);
	private:
		struct LevelData
		{
			Ogre::String nodeName;
			Ogre::Vector3 terrainOffset;
		};
		DFALevelCreator() { };
		~DFALevelCreator();

		std::vector<LevelData*> mLevels;

		static DFALevelCreator *mLevelCreator;
	};
};

#endif