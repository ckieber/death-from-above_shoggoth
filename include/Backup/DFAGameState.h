#ifndef __DFAGameState__
#define __DFAGameState__

#include <Ogre.h>

#include "DFAGameStateManager.h"

namespace DFACore
{
	class DFAGameState
	{
	public:
		virtual void enter() = 0;
		virtual void exit() = 0;

		virtual void pause() = 0;
		virtual void resume() = 0;

		virtual bool keyClicked(const OIS::KeyEvent &e) = 0;
		virtual bool keyPressed(const OIS::KeyEvent &e) = 0;
		virtual bool keyReleased(const OIS::KeyEvent &e) = 0;

		virtual bool mouseMoved(const OIS::MouseEvent &e) = 0;
		virtual bool mousePressed(const OIS::MouseEvent &e, OIS::MouseButtonID id) = 0;
		virtual bool mouseReleased(const OIS::MouseEvent &e, OIS::MouseButtonID id) = 0;

		virtual bool frameStarted(const Ogre::FrameEvent &evt) = 0;
		virtual bool frameEnded(const Ogre::FrameEvent &evt) = 0;

		void changeState(DFAGameState *state)
		{
			DFACore::DFAGameStateManager::getSingletonPtr()->changeState(state);
		}
		void pushState(DFAGameState *state)
		{
			DFACore::DFAGameStateManager::getSingletonPtr()->pushState(state);
		}
		void popState()
		{
			DFACore::DFAGameStateManager::getSingletonPtr()->popState();
		}
	protected:
		DFAGameState() { }
	};
};

#endif