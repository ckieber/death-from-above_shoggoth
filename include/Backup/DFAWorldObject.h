#ifndef __DFAWorldObject__
#define __DFAWorldObject__

#include "OgreMaxScene.hpp"

namespace DFACore
{
	class DFAWorldObject
	{
	public:
		virtual void updatePosition(const Ogre::FrameEvent &evt) = 0;
		virtual void accelerate(bool pressed) = 0;
		virtual void brake(bool pressed) = 0;
		virtual void moveLeft(bool pressed) = 0;
		virtual void moveRight(bool pressed) = 0;
		virtual void rotateLeft(bool pressed) = 0;
		virtual void rotateRight(bool pressed) = 0;
		virtual void moveUp(bool pressed) = 0;
		virtual void moveDown(bool pressed) = 0;
		
		virtual void shoot(void) = 0;
		virtual void nextWeapon(void) = 0;
		virtual void previousWeapon(void) = 0;
		virtual void selectWeapon(const int num) = 0;
		virtual int getCurrentWeapon(void) = 0;

		virtual Ogre::String getNodeName(void) = 0;
		virtual Ogre::Vector3 getPosition(void) = 0;
		virtual void setStartingPosition(const Ogre::Vector3 position) = 0;
		virtual Ogre::SceneNode* getCameraNode(void) = 0;
		virtual Ogre::String getName(void) = 0;

		virtual void createObject(const Ogre::String name, const int id,
			const Ogre::String &sceneName, Ogre::RenderWindow *renderWindow,
			OgreMax::OgreMaxScene::LoadOptions loadOptions, Ogre::SceneManager *sceneManager) = 0;
		virtual void updateAnimations(const Ogre::FrameEvent &evt) = 0;
	
	protected:
		DFAWorldObject() { }

		int mCurrentWeapon;
		Ogre::String mName;
		int mId;
	};
};

#endif