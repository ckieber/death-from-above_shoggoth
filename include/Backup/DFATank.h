#ifndef __DFATank__
#define __DFATank__

#include <Ogre.h>

#include "DFAWorldObject.h"

namespace DFAAI
{
	class DFATank : public DFACore::DFAWorldObject
	{
	public:
		DFATank();
		~DFATank();

		void updatePosition(const Ogre::FrameEvent &evt);
		void accelerate(bool pressed);
		void brake(bool pressed);
		void moveLeft(bool pressed);
		void moveRight(bool pressed);
		void rotateLeft(bool pressed);
		void rotateRight(bool pressed);
		void moveUp(bool pressed);
		void moveDown(bool pressed);

		void shoot(void);
		void nextWeapon(void);
		void previousWeapon(void);
		void selectWeapon(const int num);
		int getCurrentWeapon(void);

		Ogre::MovableObject* getTextDisplayEntity(Ogre::SceneManager *sceneMgr);
		Ogre::String getNodeName(void);
		Ogre::Vector3 getPosition(void);
		void setStartingPosition(const Ogre::Vector3 position);
		Ogre::SceneNode* getCameraNode(void);
		Ogre::String getName(void);

		void createObject(const Ogre::String name, const int id,
			const Ogre::String &sceneName, Ogre::RenderWindow *renderWindow,
			OgreMax::OgreMaxScene::LoadOptions loadOptions, Ogre::SceneManager *sceneManager);
		void updateAnimations(const Ogre::FrameEvent &evt);

	private:
		void loadAnimations(Ogre::SceneManager *sceneManager);
		void loadSounds();

		OgreMax::OgreMaxScene *mTankScene;
		Ogre::SceneNode *mTankNode;
		Ogre::SceneNode *mMovementNode;
		Ogre::SceneNode *mCameraNode;
	};
};

#endif