#ifndef __DFAAvatar__
#define __DFAAvatar__

#include <Ogre.h>
//#include <OgreSingleton.h>

#include "DFAWorldObject.h"

namespace DFACore
{
	class DFAAvatar : public DFAWorldObject//, public Ogre::Singleton<DFAAvatar>
	{
	public:
		void updatePosition(const Ogre::FrameEvent &evt);
		void accelerate(bool pressed);
		void brake(bool pressed);
		void moveLeft(bool pressed);
		void moveRight(bool pressed);
		void rotateLeft(bool pressed);
		void rotateRight(bool pressed);
		void moveUp(bool pressed);
		void moveDown(bool pressed);

		void shoot(void);
		void nextWeapon(void);
		void previousWeapon(void);
		void selectWeapon(const int num);
		int getCurrentWeapon(void);
		void setTargetList(std::multimap<DFACore::DFAWorldObject*, double> &targetList);

		Ogre::String getNodeName(void);
		Ogre::Vector3 getPosition(void);
		double getMaxTargetRange(void);
		void setStartingPosition(const Ogre::Vector3 position);
		void lookAt(const Ogre::Vector3 position);
		Ogre::SceneNode* getCameraNode(void);
		Ogre::String getName(void);

		void createObject(const Ogre::String name, const int id,
			const Ogre::String &sceneName, Ogre::RenderWindow *renderWindow,
			OgreMax::OgreMaxScene::LoadOptions loadOptions, Ogre::SceneManager *sceneManager);

		void updateAnimations(const Ogre::FrameEvent &evt);
		bool isDoorOpen() const;
		bool isGearUp() const;
		void doorsOpen(bool open);
		void gearUp(bool up);

		static DFAAvatar* getSingletonPtr(void);
	private:
		DFAAvatar();
		~DFAAvatar();
		void loadAnimations(const int id, Ogre::SceneManager *sceneManager);
		void loadSounds();

		void openDoors();
		void closeDoors();
		void gearUp();
		void gearDown();

		Ogre::Real mSpeed;
		Ogre::Real mAnglePitch;
		Ogre::Real mAngleRoll;
		Ogre::Real mAngleYaw;
		Ogre::Real mPitchSpeed;
		Ogre::Real mRollSpeed;
		Ogre::Real mYawSpeed;

		bool mBrake, mAccelerate;
		bool mRollLeft, mRollRight;
		bool mYawLeft, mYawRight;

		bool mDoorsOpen;
		bool mGearUp;

		Ogre::Vector3 mDirection;
		Ogre::Vector3 mClimb;
		std::list<Ogre::AnimationState*> mAnimationStates;

		OgreMax::OgreMaxScene *mAvatarScene;
		Ogre::SceneNode *mAvatarNode;
		Ogre::SceneNode *mMovementNode;
		Ogre::SceneNode *mCameraNode;

		std::multimap<DFACore::DFAWorldObject*, double> mTargetList;

		static DFAAvatar *mAvatar;
	};
};

#endif