#ifndef __DFALevelCreator__
#define __DFALevelCreator__

#include <Ogre.h>

#include "DFAWorldObject.h"
#include "OgreOggSound.h"

namespace DFAData
{
	class DFALevelCreator
	{
	public:
		std::list<DFACore::DFAWorldObject*> createLevel(const int levelNumber, Ogre::SceneManager *sceneMgr, Ogre::RenderWindow *renderWindow,
												OgreOggSound::OgreOggSoundManager *soundManager, Ogre::Camera *camera);
		static DFALevelCreator* getSingletonPtr(void);
	private:
		DFALevelCreator() { };
		~DFALevelCreator();

		static DFALevelCreator *mLevelCreator;
	};
};

#endif