#ifndef __DFALogoState__
#define __DFALogoState__

#include <Ogre.h>

#include "DFAGameState.h"
#include "DFAFader.h"

namespace DFACore
{
	class DFALogoState : public DFAGameState, public DFATools::DFAFaderCallback
	{
	public:
		void enter();
		void exit();

		void pause();
		void resume();

		bool keyClicked(const OIS::KeyEvent &e);
		bool keyPressed(const OIS::KeyEvent &e);
		bool keyReleased(const OIS::KeyEvent &e);

		bool mouseMoved(const OIS::MouseEvent &e);
		bool mousePressed(const OIS::MouseEvent &e, OIS::MouseButtonID id);
		bool mouseReleased(const OIS::MouseEvent &e, OIS::MouseButtonID id);

		bool frameStarted(const Ogre::FrameEvent &evt);
		bool frameEnded(const Ogre::FrameEvent &evt);

		void fadeInCallback();
		void fadeOutCallback();

		static DFALogoState* getInstance()
		{
			return &mLogoState;
		}
	protected:
		DFALogoState() { }

		Ogre::Root *mRoot;
		Ogre::SceneManager *mSceneMgr;
		Ogre::Viewport *mViewport;
		Ogre::Camera *mCamera;
		OIS::Keyboard *mKeyboard;
		OIS::Mouse *mMouse;
		Ogre::Overlay *mLogo;
		DFATools::DFAFader *mFader;
		bool mExitGame, mLogoFadeDone;
	private:
		static DFALogoState mLogoState;
	};
};

#endif