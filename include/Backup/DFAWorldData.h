#ifndef __DFAWorldData__
#define __DFAWorldData__

#include <Ogre.h>
#include <map>

#include "DFAWorldObject.h"

namespace DFAData
{
	class DFAWorldData
	{
	public:
		std::multimap<DFACore::DFAWorldObject*, double> updateTargetList(const Ogre::FrameEvent &evt, std::list<DFACore::DFAWorldObject*> &worldObjects, Ogre::SceneManager *sceneMgr);

		static DFAWorldData* getSingletonPtr(void);
	private:
		DFAWorldData();
		~DFAWorldData();

		std::multimap<DFACore::DFAWorldObject*, double> mDistanceData;

		static DFAWorldData *mWorldData;
	};
};

#endif