#ifndef __DFASceneObject__
#define __DFASceneObject__

#include "DFAWorldObject.h"
#include "OgreMaxScene.hpp"

namespace DFACore
{
	class DFASceneObject
	{
	public:
		DFASceneObject() { }
		virtual ~DFASceneObject() { }

		virtual void decreaseHealth(const Ogre::Real damage) = 0;

		virtual Ogre::String getNodeName(void) = 0;
		virtual Ogre::Vector3 getPosition(void) = 0;
		virtual void setPosition(const Ogre::Vector3 position) = 0;
		virtual Ogre::SceneNode* getCameraNode(void) = 0;
		virtual QueryMask getQueryMask(void) = 0;
		virtual OgreMax::OgreMaxScene::ObjectExtraDataMap& getObjectExtraDataMap(void) = 0;

		virtual void createObject(const Ogre::String name, const int id,
			const Ogre::String &sceneName, Ogre::RenderWindow *renderWindow,
			OgreMax::OgreMaxScene::LoadOptions loadOptions, Ogre::SceneManager *sceneManager,
			const QueryMask queryMask) = 0;
	
	protected:
		virtual void updatePosition(const Ogre::Real frameRate) = 0;
		virtual void updateAnimations(const Ogre::Real frameRate) = 0;
		virtual void loadAnimations(Ogre::SceneManager *sceneManager) = 0;
		virtual void loadSounds() = 0;

		DFATools::DFACollisionTool *mCollisionTool;
		Ogre::Real mHealth;
		QueryMask mQueryMask;
		Ogre::String mNamePrefix;

		OgreMax::OgreMaxScene *mDotSceneNode;
		Ogre::SceneNode *mObjectNode;
		Ogre::SceneNode *mMovementNode;
		Ogre::SceneNode *mCameraNode;
	};
};

#endif