#ifndef __DFAPauseState__
#define __DFAPauseState__

#include <Ogre.h>

#include "DFAGameState.h"

namespace DFACore
{
	class DFAPauseState : public DFAGameState
	{
	public:
		void enter();
		void exit();

		void pause();
		void resume();

		bool keyClicked(const OIS::KeyEvent &e);
		bool keyPressed(const OIS::KeyEvent &e);
		bool keyReleased(const OIS::KeyEvent &e);

		bool mouseMoved(const OIS::MouseEvent &e);
		bool mousePressed(const OIS::MouseEvent &e, OIS::MouseButtonID id);
		bool mouseReleased(const OIS::MouseEvent &e, OIS::MouseButtonID id);

		bool frameStarted(const Ogre::FrameEvent &evt);
		bool frameEnded(const Ogre::FrameEvent &evt);

		void captureInput(void);
		void processLogic(const Ogre::Real frameRate);

		static DFAPauseState* getInstance()
		{
			return &mPauseState;
		}
	protected:
		DFAPauseState() { }

		Ogre::Root *mRoot;
		Ogre::SceneManager *mSceneMgr;
		Ogre::Viewport *mViewport;
		Ogre::Camera *mCamera;
		OIS::Keyboard *mKeyboard;
		OIS::Mouse *mMouse;
	private:
		static DFAPauseState mPauseState;
	};
};
#endif