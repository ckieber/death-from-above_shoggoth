#ifndef __DFAWorldObject__
#define __DFAWorldObject__

#include "DFACollisionTool.h"

namespace DFACore
{
	enum QueryMask
	{
		AVATAR = 1<<0,
		FRIENDLY = 1<<1,
		ENEMY = 1<<2,
		STATIONARY = 1<<3,
		WEAPON = 1<<4
	};

	class DFAWorldObject
	{
	public:
		DFAWorldObject() { }
		virtual ~DFAWorldObject() { }

		virtual void update(const Ogre::Real frameRate, Ogre::SceneManager *sceneManager) = 0;
		virtual bool isDestroyed(void) = 0;
		virtual Ogre::String getName(void) = 0;
	
	protected:
		bool mDestroyed;
		bool mPlayDestroyedAnimation;
		Ogre::String mName;
		int mId;
	};
};

#endif