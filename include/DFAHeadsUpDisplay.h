#ifndef __DFAHeadsUpDisplay__
#define __DFAHeadsUpDisplay__

#include <Ogre.h>

namespace DFAData
{
	class DFAHeadsUpDisplay
	{
	public:
		

		static DFAHeadsUpDisplay* getSingletonPtr(void);
	private:
		DFAHeadsUpDisplay();
		~DFAHeadsUpDisplay();

		static DFAHeadsUpDisplay *mHeadsUpDisplay;
	};
};

#endif