#ifndef __DFAFrameManager__
#define __DFAFrameManager__

#include <Ogre.h>

namespace DFAData
{
	class DFAFrameManager
	{
	public:
		void update(void);
		void setFrameUpdateFrequency(const int updateFrequency);
		bool isInUpdateFrame(const int frameNumber);

		static DFAFrameManager* getSingletonPtr(void);
	protected:
		DFAFrameManager();
		~DFAFrameManager();

		int mCurrentFrameNumber;
		int mUpdateFrequency;
	
	private:
		static DFAFrameManager *mFrameManager;
	};
};

#endif